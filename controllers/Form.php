<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

	/**
	* Get's the credentials for the API
	*
	* These credentials are used by the API to determine if the user has access
	* to the endpoint.
	*
	* @access private
	* @return array
	*/
	private function _get_api_credentials(){
		return array(
			'ACCOUNT_id'=> $_SESSION['ACCOUNT_id'],
			'token' => $_SESSION['token']
		);
	}


	/**
	* Makes and API Call to get the application details
	*
	* If the request fails it returns FALSE.  Otherwise it returns the decoded
	* JSON object.
	*
	* @access private
	* @return mixed
	*/
	private function _get_application_details(){
		# Get the credentials
		$data['api_credentials'] = $this->_get_api_credentials();
		# Request the information from the API
		$response = Requests::post($this->config->item('api_url').'v2/application-details/'.$_SESSION['APPLICATION_id'], array(), $data);
		# Return the API response
		return($this->_handle_api_response($response));
	}


	/**
	* Makes and API Call to get the ecclesiastical leader details
	*
	* @access private
	* @param int $id The ecclesiastical leader id
	* @return mixed
	*/
	private function _get_ecclesiastical_leader_details($id){
		$response = Requests::get($this->config->item('api_url').'v2/ecclesiastical-leader/'.$id);
		# Return the API response
		return($this->_handle_api_response($response));
	}


	/**
	* Makes and API Call to get the application details for all household members
	*
	* @access private
	* @return mixed
	*/
	private function _get_household_applications(){
		# Get the API credentials
		$data['api_credentials'] = $this->_get_api_credentials();
		# Request the data from the API
		$response = Requests::post($this->config->item('api_url').'v2/household-application-details/'.$_SESSION['APPLICATION_id'], array(), $data);
		# Return the API response
		return($this->_handle_api_response($response));
	}


	/**
	* Get's the HCP Start date
	*
	* @access private
	* @return date
	*/
	private function _get_hcp_start_date(){
		//return (strtotime('6 July 2018'));
		$d = new DateTime('07-06-2019'); # mm-dd-yyyy
		$timestamp = $d->getTimestamp(); # Unix timestamp
		return($d->format('Y-m-d'));
	}


	/**
	* Makes and API Call to get the application percent complete
	*
	* @access private
	* @param integer $APPLICATION_id The id for the application
	* @return mixed
	*/
	private function _get_percent_complete($APPLICATION_id){
		# Get the API credentials
		$data['api_credentials'] = $this->_get_api_credentials();
		# Request the data from the API
		$response = Requests::post($this->config->item('api_url').'v2/application/'.$APPLICATION_id.'/percent-complete', array(), $data);
		# Return the API response
		return($this->_handle_api_response($response));
	}


	/**
	* Makes and Series of API Calls to get the application percent complete for
	* all applications within the household
	*
	* @access private
	* @param mixed $household_apps FALSE or the array with application detials
	* @return mixed
	*/
	private function _get_household_percent_complete($household_apps){
		$percent_complete = array();
		if($household_apps){
			foreach ($household_apps as $key => $app) {
				$percent_complete[$key] = $this->_get_percent_complete($app->id);
			}
		}

		if(sizeof($percent_complete)>0){
			return($percent_complete);
		} else {
			return(FALSE);
		}
	}

	/**
	* Get the current pageant application year
	*
	* @access private
	* @return integer
	*/
	private function _get_current_application_year(){
		return((int)$_SESSION['current_application_year']);
	}

	/**
	* Get the current pageant year
	*
	* @access private
	* @return integer
	*/
	private function _get_current_pageant_year(){
		return((int)$_SESSION['current_pageant_year']);
	}


	/**
	* Get the ecclesiastical leader suggestions
	*
	* @access private
	* @return mixed
	*/
	private function _get_ecclesiastical_leader_suggestions($APPLICATION_id){
		# This is the API endpoint
		$api_url = $this->config->item('api_url').'v2/ecclesiastical-leader/suggestions/'.$APPLICATION_id;
		# Get the API credentials
		$_POST['api_credentials'] = $this->_get_api_credentials();
		# Request the relatives from the API
		$response = Requests::post($api_url, array(), $_POST);
		# Return the API response
		$response = $this->_handle_api_response($response);
		return($response->suggestions);
	}


	/**
	* Get the past pageant application years
	*
	* @access private
	* @return mixed
	*/
	private function _get_past_pageants(){
		# Pull the application details
		$response = Requests::get($this->config->item('api_url').'v2/individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'/past_pageants');
		$response = $this->_handle_api_response($response);
		return($response);
	}


	/**
	* Get's the relatives for an individual
	*
	* @access private
	* @param int $INDIVIDUAL_id
	* @return date
	*/
	private function _get_relatives($INDIVIDUAL_id){
		# This is the API endpoint
		$api_url = $this->config->item('api_url').'v2/individual/'.$INDIVIDUAL_id.'/relatives';
		# Get the API credentials
		$_POST['api_credentials'] = $this->_get_api_credentials();
		# Request the relatives from the API
		$response = Requests::post($api_url, array(), $_POST);
		# Return the API response
		return($this->_handle_api_response($response));
	}


	/**
	* Generalized function to return an API response
	*
	* This function takes the response object from PHP Requests and checks if data
	* exists.  If it does it will return it.  If it doesn't it returns FALSE
	*
	* @access private
	* @param object $response
	* @return mixed
	*/
	private function _handle_api_response($response){
		if($response->status_code == 200){
			# Valid API response so decode JSON object
			$json = json_decode($response->body);
			# if data exists return it
			if(isset($json->data)){
				return($json->data);
			}
		}
		# Request failed so return FALSE
		return(FALSE);
	}


	/**
	* Generalized function to handle the save and continue form post
	*
	* @access private
	* @return object
	*/
	private function _save_POST_via_api(){
		# Get the API URL
		$api_url = $this->config->item('api_url').'v2/application-details/'.$_SESSION['APPLICATION_id'];
		# Get the API credentials
		$_POST['api_credentials'] = $this->_get_api_credentials();
		# Get the individual id
		$_POST['APPLICATION_INDIVIDUAL_id'] = $_SESSION['APPLICATION_INDIVIDUAL_id'];
		# Make the API call to update the record
		$response = Requests::post($api_url, array(), $_POST);
		# Return the API response object
		return($response);
	}

	private function _participation_agreement_check($percent_complete){
		if((int)$percent_complete->preselection->terms_and_conditions == 0){
			redirect('form/participation-agreement');
		}
	}

	private function _get_workcrew_eligible($id){
		# This is the API endpoint
		$api_url = $this->config->item('api_url').'v2/individual/'.$id.'/workcrew-eligible';
		# Get the API credentials
		$_POST['api_credentials'] = $this->_get_api_credentials();
		# Request the relatives from the API
		$response = Requests::post($api_url, array(), $_POST);
		# Return the API response
		return($this->_handle_api_response($response));
	}

	/**
	* Check to see if the user is signed in
	*
	* This checks for the signed_in session variable.  This should be set if they
	* went through the SSO process (see the account repository)
	*
	* @access private
	* @param boolean check_accepting_applications Should we check if we are
	* accepting applications.  TRUE by default.
	* @return null
	*/
	private function _sign_in_check($check_accepting_applications = TRUE){
		if($check_accepting_applications){
			$accepting_applications = $this->_get_accepting_applications();
		} else {
			$accepting_applications  = TRUE;
		}

		if(!isset($_SESSION['signed_in']) || !$accepting_applications){
			# The user is not signed in (or their session expired).  Send the user to
			# SSO form but set the goto flash variable first with the current_url.
			$url = current_url();
			if(!isset($_SESSION['signed_in'])){
				$this->session->set_flashdata('goto', $url);
			}
			$sso = $this->config->item('sso_url');
			redirect($sso);
		}
	}


	private function _get_accepting_applications(){
		$accepting_applications = $this->config->item('accepting_applications');
		$mode = $this->config->item('mode');
		$access_override = FALSE;

		if(isset($_SESSION['access_override'])  && $_SESSION['access_override']){
			foreach($_SESSION['access_override'] as $key=>$url){
				if ($url == base_url()){
					$access_override = TRUE;
				}
			}
		}

		if($mode == "application"){
			return(TRUE);
		} elseif ($mode == "selection") {
			if(!$access_override){
				$this->session->set_userdata('error', 'The application deadline has passed.');
			}
			return($access_override);
		} elseif ($mode == "post-selection") {
			if($access_override){
				return(TRUE);
			} /*else {
				$this->session->set_userdata('error', 'The application deadline has passed.');
			}*/
		}

		return($accepting_applications);
	}


	/**
	* Check to see if the user is has the application id set.  If it is not set
	* Then it will redirect them to the index method to set it automatically.
	*
	* @access private
	* @return null
	*/
	private function _application_id_check(){
		# This is needed for the case that someone times out then signs back in
		if(!isset($_SESSION['APPLICATION_id'])){
			redirect('form');
		}
	}


	/**
	* Add a historic relative to this application
	*
	* The user submitted a form from the dashboard indicating they would like to
	* add a relative to their application.  This method will add the individual
	* to their application and return them to the dashboard where they can add
	* another individual or edit and existing application.
	*
	* @access public
	* @return null
	*/
	public function add_relative(){
		$this->_sign_in_check();
		$this->_application_id_check();

		# Get the current pageant year
		$data['pageant_year'] = $this->_get_current_application_year();

		# Get the api credentails
		$data['api_credentials'] = $api_credentials = $this->_get_api_credentials();

		# Start the application data
		$data['status'] = 'Started';
		$data['apply_for_cast'] = 1;
		$data['INDIVIDUAL_id'] = $this->input->post('INDIVIDUAL_id');
		$data['HOUSEHOLD_id'] = $this->input->post('HOUSEHOLD_id');

		# Create a new application record
		$response = Requests::post($this->config->item('api_url').'v2/application/', array(), $data);

		# Create application history record
		$row = $this->_handle_api_response($response);
		$history = array('api_credentials'=>$api_credentials, 'APPLICATION_id'=>$row->id, 'action'=>'Started application', 'action_taken_by'=>$_SESSION['INDIVIDUAL_id'], 'notes'=>'Application started by '.$_SESSION['first_name'].' '.$_SESSION['last_name']);
		$response = Requests::post($this->config->item('api_url').'v2/application-history/', array(), $history);

		# Redirect to the dashboard
		redirect('dashboard');
	}


	/**
	* Gather contact information
	*
	* @access public
	* @return null
	*/
	public function contact_information(){
		# Check to see if the user is signed in
		$this->_sign_in_check();
		$this->_application_id_check();

		# Check to see if there is a form POST
		$save_and_continue = $this->input->post('save_and_continue');
		if($save_and_continue){
			# Save the POST variables by and API call
			//$response = $this->_save_POST_via_api();
			# We have saved.  Let's continue!
			redirect('form/ecclesiastical-leader');
		}

		# Get the application data and display it
		$data['workcrew_eligible'] = $this->_get_workcrew_eligible($_SESSION['APPLICATION_INDIVIDUAL_id']);
		$data['application_id'] = $_SESSION['APPLICATION_id'];
		$data['application_details'] = $this->_get_application_details();
		$data['percent_complete'] = $percent_complete = $this->_get_percent_complete($_SESSION['APPLICATION_id']);
		$this->_participation_agreement_check($percent_complete);
		$data['body_class'] = 'contact_information';
		$data['title'] = 'Personal Information';
		$data['sidebar'] = $this->load->view('form/sidebar', $data, TRUE);
		$data['application'] = $this->load->view('form/contact_information', $data, TRUE);
		$this->load->view('template', $data);
	}


	/**
	* This presents the status of the applications and allows creating/editing
	*
	* @access public
	* @return none
	*/
	public function dashboard(){
		# Check to see if the user is signed in
		$household_apps = $this->_get_household_applications();
		$this->_sign_in_check();
		$this->_application_id_check();

		# Check for POST
		$action = $this->input->post('action');
		if($action){
			# Get the other two application variables
			$APPLICATION_id = $this->input->post('APPLICATION_id');
			$APPLICATION_INDIVIDUAL_id = $this->input->post('APPLICATION_INDIVIDUAL_id');

			# Determine what action to take
			switch ($action) {
				case 'edit':
				# The user wants to edit so mark the application status as started
				# (clears out reviewed and submitted applications)
				# Get the API credentials
				$api_credentials = $this->_get_api_credentials();
				$status_update = array('api_credentials' => $api_credentials, 'status'=>'Started');
				# Make the API call to update the record
				//$response = Requests::post($this->config->item('api_url').'v2/application/'.$_SESSION['APPLICATION_id'], array(), $status_update);
				# We have updated the status.  Now update the application history.
				$history = array('api_credentials'=>$api_credentials, 'APPLICATION_id'=>$_SESSION['APPLICATION_id'], 'action'=>'Application in edit mode', 'action_taken_by'=>$_SESSION['INDIVIDUAL_id'],'notes'=>'Application possibly edited by '.$_SESSION['first_name'].' '.$_SESSION['last_name']);
				$response = Requests::post($this->config->item('api_url').'v2/application-history/', array(), $history);

				# set the session variables and redirect
				$_SESSION['APPLICATION_id'] = $APPLICATION_id;
				$_SESSION['APPLICATION_INDIVIDUAL_id'] = $APPLICATION_INDIVIDUAL_id;
				# Pull the application
				$app = $this->_get_application_details();
				$_SESSION['application_full_name'] = $app->first_name.' '.$app->last_name;

				redirect('form/participation-agreement');
				break;

				case 'delete':
				# The user wants to delete an application so make a post to the API
				$delete['api_credentials'] = $this->_get_api_credentials();
				# Pull the application details
				$response = Requests::post($this->config->item('api_url').'v2/application-details/'.$_POST['APPLICATION_id'], array(), $delete);
				$app = $this->_handle_api_response($response);
				# Let's delete the application
				$delete['deleted'] = 1;
				$response = Requests::post($this->config->item('api_url').'v2/application/'.$_POST['APPLICATION_id'], array(), $delete);
				$data['success_message'] = $app->first_name.' '.$app->last_name."'s application was successfully deleted!";
				break;

				case 'submit':
				# The user wants to submit the applications
				$application_ids = $this->input->post('application_ids');
				# The user wants to submit an application so make a post to the API
				$submit['api_credentials'] = $api_credentials = $this->_get_api_credentials();
				$submit['status'] = 'Submitted';
				foreach(explode('|',$application_ids) as $key => $app_id){
					$response = Requests::get($this->config->item('api_url').'v2/application/'.$app_id);
					$app = $this->_handle_api_response($response);
					if($app->cast == 1 || $app->workcrew == 1 || $app->staff == 1){
						$submit['status'] = 'Selected';
					}
					$response = Requests::post($this->config->item('api_url').'v2/application/'.$app_id, array(), $submit);
					# Add application history record
					$history = array('api_credentials'=>$api_credentials, 'APPLICATION_id'=>$app_id, 'action'=>'Submitted application', 'action_taken_by'=>$_SESSION['INDIVIDUAL_id'],'notes'=>'Application submitted by '.$_SESSION['first_name'].' '.$_SESSION['last_name']);
					$response = Requests::post($this->config->item('api_url').'v2/application-history/', array(), $history);
					# Send off Ecclesiastical Endorsement request
					# https://hcpageant.com/ecclesiastical-endorsement/action/send-first-email/ECCLESIASTICAL_LEADER_id
					if($app->date_ecclesiastical_leader_emailed == NULL){
						$response = Requests::get('https://hcpageant.com/ecclesiastical-endorsement/action/send-first-email/'.$app->ECCLESIASTICAL_LEADER_id);
					}
				};
				break;
			}
		}

		# Get the application information and send it to the views
		$data['accepting_applications'] = $this->_get_accepting_applications();
		$data['application_id'] = $_SESSION['APPLICATION_id'];
		$data['applications'] = $household_apps;
		$data['percent_complete'] = $this->_get_household_percent_complete($household_apps);
		$data['relatives'] = $this->_get_relatives($_SESSION['INDIVIDUAL_id']);
		$data['body_class'] = 'application_dashboard';
		$data['title'] = 'Application Dashboard';
		$data['sidebar'] = $this->load->view('form/sidebar', $data, TRUE);
		$data['application'] = $this->load->view('form/dashboard', $data, TRUE);
		$this->load->view('template', $data);
	}

	/**
	* Gather information about the ecclesiastical leader
	*
	* @access public
	* @return null
	*/
	public function ecclesiastical_leader(){
		# Check to see if the user is signed in
		$this->_sign_in_check();
		$this->_application_id_check();

		# Check to see if there is a form POST variables
		$action = $this->input->post('action');
		$save_and_continue = $this->input->post('save_and_continue');

		if($action == 'remove'){
			# Remove the association with the ecclesiastical leader
			$data['ECCLESIASTICAL_LEADER_id'] = 'NULL ME';
		}

		if($action == 'add'){
			# Add the association with the ecclesiastical leader
			$data['ECCLESIASTICAL_LEADER_id'] = $this->input->post('ECCLESIASTICAL_LEADER_id');
		}

		if($action == 'add' || $action == 'remove'){
			# Remove the association with the ecclesiastical leader
			$data['api_credentials'] = $this->_get_api_credentials();
			$response = Requests::post($this->config->item('api_url').'v2/application/'.$_SESSION['APPLICATION_id'], array(), $data);
			if(!$response->status_code == 200){
				$data['error_message'] = json_decode($response->body)->data->error_message;
			}
		}

		if($save_and_continue){
			# Get the API URL
			$api_url = $this->config->item('api_url').'v2/application/'.$_SESSION['APPLICATION_id'];
			# Get the API credentials
			$_POST['api_credentials'] = $this->_get_api_credentials();
			# Get the individual id
			$_POST['APPLICATION_INDIVIDUAL_id'] = $_SESSION['APPLICATION_INDIVIDUAL_id'];
			# Make the API call to update the record
			Requests::post($api_url, array(), $_POST);
			# Move to the next step
			redirect('form/experience');
		}

		# Get the data and send it to the view
		$data['application_id'] = $_SESSION['APPLICATION_id'];
		$data['ecclesiastical_leader_suggestions'] = $this->_get_ecclesiastical_leader_suggestions($_SESSION['APPLICATION_id']);
		$data['percent_complete'] = $percent_complete = $this->_get_percent_complete($_SESSION['APPLICATION_id']);
		$this->_participation_agreement_check($percent_complete);
		$data['application_details'] = $application_details = $this->_get_application_details();
		$data['ecclesiastical_leader_details'] = ($application_details)? $this->_get_ecclesiastical_leader_details($application_details->ECCLESIASTICAL_LEADER_id) : FALSE;
		$data['body_class'] = 'ecclesiastical_leader';
		$data['title'] = 'Ecclesiastical Leader';
		$data['sidebar'] = $this->load->view('form/sidebar', $data, TRUE);
		$data['application'] = $this->load->view('form/ecclesiastical_leader', $data, TRUE);
		$this->load->view('template', $data);
	}


	/**
	* Gather's personal experience
	*
	* @access public
	* @return null
	*/
	public function experience(){
		# Check to see if the user is signed in
		$this->_sign_in_check();
		$this->_application_id_check();

		# Check to see if there is a form POST
		$save_and_continue = $this->input->post('save_and_continue');
		if($save_and_continue){
			# Save the POST variables by and API call
			$response = $this->_save_POST_via_api();
			# We have saved.  Let's continue!
			redirect('form/misc');
		}

		# Get the past pageants
		$data['past_pageants'] = $this->_get_past_pageants();
		$data['pageant_year'] = $this->_get_current_application_year();
		# Get data and send it to the view
		$data['application_id'] = $_SESSION['APPLICATION_id'];
		$data['percent_complete'] = $percent_complete = $this->_get_percent_complete($_SESSION['APPLICATION_id']);
		$this->_participation_agreement_check($percent_complete);
		$data['application_details'] = $this->_get_application_details();
		$data['body_class'] = 'experience';
		$data['title'] = 'Experience';
		$data['sidebar'] = $this->load->view('form/sidebar', $data, TRUE);
		$data['application'] = $this->load->view('form/experience', $data, TRUE);
		$this->load->view('template', $data);
	}


		/**
		* Sets the head of household flag then redirects to dashboard
		*
		* @access public
		* @return null
		*/
		public function head_of_household() {
			# Check to see if the user is signed in and redirect to SSO if not
			$this->_sign_in_check();
			$data['api_credentials'] = $this->_get_api_credentials();
			$data['HOUSEHOLD_id'] = $this->uri->segment(3);
			$data['INDIVIDUAL_id'] = $this->uri->segment(4);
			$data['APPLICATION_id'] = $_SESSION['APPLICATION_id'];
			$response = Requests::post($this->config->item('api_url').'v2/head-of-household/', array(), $data);
			redirect('dashboard');
		}


	/**
	* Redirect to the user to the proper location
	*
	* @access public
	* @return null
	*/
	public function index() {
		# Check to see if the user is signed in and redirect to SSO if not
		$this->_sign_in_check();

		# Check to see if the user has an application started.  If so redirect them
		# to the application dashboard.
		$data['api_credentials'] = $this->_get_api_credentials();
		$response = Requests::post($this->config->item('api_url').'v2/individual/'.$_SESSION['INDIVIDUAL_id'].'/current-year-application', array(), $data);
		if($response->status_code == 200){
			# Set the two session variables needed for the application process
			$_SESSION['APPLICATION_id'] = json_decode($response->body)->data->id;
			$_SESSION['APPLICATION_INDVIDUAL_id'] = $_SESSION['INDIVIDUAL_id'];
			# Redirect to the dashboard
			redirect('dashboard');
		}

		# They don't have one started so let's start one then redirect to the
		# application dashboard
		redirect('start');
	}


	/**
	* Hole that allows late applicants
	*
	* @access public
	* @return null
	*/
	public function late_applicants(){
		if(isset($_SESSION['signed_in'])){
			$_SESSION['access_override'][] = base_url();
		}
		$this->_sign_in_check(FALSE);

		redirect('start');
	}



	/**
	* Gather miscellaneous information prior to submission
	*
	* @access public
	* @return null
	*/
	public function misc(){
		# Check to see if the user is signed in
		$this->_sign_in_check();
		$this->_application_id_check();

		# Check to see if this step needs to be skipped
		$app = $this->_get_application_details();
		if($app->apply_for_staff == 0 && $app->apply_for_cast == 0 && $app->apply_for_workcrew == 1){
			redirect('form/review');
		}

		# Check to see if there is a form POST
		$save_and_continue = $this->input->post('save_and_continue');
		if($save_and_continue){
			# We need to change the checkboxes
			$_POST['boys_girls_4_8'] = (isset($_POST['boys_girls_4_8']))?1:0;
			$_POST['girls_9'] = (isset($_POST['girls_9']))?1:0;
			$_POST['boys_9'] = (isset($_POST['boys_9']))?1:0;
			$_POST['girls_10_11'] = (isset($_POST['girls_10_11']))?1:0;
			$_POST['boys_10_11'] = (isset($_POST['boys_10_11']))?1:0;
			$_POST['girls_12_13'] = (isset($_POST['girls_12_13']))?1:0;
			$_POST['boys_12_13'] = (isset($_POST['boys_12_13']))?1:0;
			$_POST['youth_14_15'] = (isset($_POST['youth_14_15']))?1:0;
			$_POST['youth_16_17'] = (isset($_POST['youth_16_17']))?1:0;
			$_POST['young_single_adult'] = (isset($_POST['young_single_adult']))?1:0;
			$_POST['adult_cast_team'] = (isset($_POST['adult_cast_team']))?1:0;

			# Save the POST variables by and API call
			$response = $this->_save_POST_via_api();
			# We have saved.  Let's continue!
			redirect('form/review');
		}

		# Get data for the view
		$data['hcp_start_date'] = $this->_get_hcp_start_date();
		$data['percent_complete'] = $percent_complete = $this->_get_percent_complete($_SESSION['APPLICATION_id']);
		$this->_participation_agreement_check($percent_complete);
		$data['application_id'] = $_SESSION['APPLICATION_id'];
		$data['application_details'] = $app;
		$data['applying_for'] = applying_for($app);
		$data['body_class'] = 'miscellaneous';
		$data['title'] = 'Miscellaneous Questions';
		$data['sidebar'] = $this->load->view('form/sidebar', $data, TRUE);
		$data['application'] = $this->load->view('form/miscellaneous', $data, TRUE);
		$this->load->view('template', $data);
	}

	/**
	* Participation agreements form page
	*
	* @access public
	* @return none
	*/
	public function participation_agreement(){
		# Check to see if the user is signed in
		$this->_sign_in_check();
		# Check for a valid application id
		$this->_application_id_check();

		# Check to see if there is a form POST
		$save_and_continue = $this->input->post('save_and_continue');
		if($save_and_continue){
			# Assume we should redirect
			$redirect = TRUE;
			# Inspect the checkbox values and translate them into 1 and 0 flags.
			$_POST['participation_agreement_1'] = (isset($_POST['participation_agreement_1']))?1:0;
			//$_POST['apply_for_workcrew'] = (isset($_POST['apply_for_workcrew']))?1:0;
			//$_POST['apply_for_staff'] = (isset($_POST['apply_for_staff']))?1:0;
			# Sanity Checks

			# Work crew sanity checks
			$age_at_pageant = $this->_get_current_application_year() - ((int) substr($_POST['date_of_birth'], 0, 4));
			if($_POST['apply_for_workcrew'] && ($_POST['gender'] == 'F' || !$_POST['single'] || $age_at_pageant < 15 || $age_at_pageant > 19)){
				$_POST['apply_for_workcrew'] = 0;
				$data['error_message'] = $_POST['first_name'].' '.$_POST['last_name'].' is not eligible for workcrew';
				$redirect = FALSE;
			}

			# Save the POST variables by and API call
			//$response = $this->_save_POST_via_api();

			# Check to see if we need to change session variables
			if($_SESSION['APPLICATION_INDIVIDUAL_id'] == $_SESSION['INDIVIDUAL_id']){
				$_SESSION['first_name'] = $_POST['first_name'];
				$_SESSION['last_name'] = $_POST['last_name'];
			}
			$_SESSION['application_full_name'] = $_POST['first_name'].' '.$_POST['last_name'];
			# We have saved.  Let's continue!
			if($redirect){
				redirect('form/preliminary-questions');
			}
		}

		# Check if they were redirected with an error message
		$error_message = $this->session->flashdata('error_message');
		if($error_message){
			$data['error_message'] = $error_message;
		}

		# Pull the application details from the API into the view
		$data['application_details'] = $this->_get_application_details();

		# Pass other variables into the view
		$data['application_id'] = $_SESSION['APPLICATION_id'];
		$data['percent_complete'] = $pc = $this->_get_percent_complete($_SESSION['APPLICATION_id']);
		$data['body_class'] = 'participation_agreement';
		$data['title'] = 'Participation Agreement';

		# Load the views
		$data['sidebar'] = $this->load->view('form/sidebar', $data, TRUE);
		$data['application'] = $this->load->view('form/participation_agreement', $data, TRUE);
		$this->load->view('template', $data);
	}


	/**
	* Preliminary questions form page
	*
	* @access public
	* @return none
	*/
	public function preliminary_questions(){
		# Check to see if the user is signed in
		$this->_sign_in_check();
		# Check for a valid application id
		$this->_application_id_check();

		# Check to see if there is a form POST
		$save_and_continue = $this->input->post('save_and_continue');
		if($save_and_continue){
			# Assume we should redirect
			$redirect = TRUE;
			# Inspect the checkbox values and translate them into 1 and 0 flags.
			$_POST['apply_for_cast'] = (isset($_POST['apply_for_cast']))?1:0;
			$_POST['apply_for_workcrew'] = (isset($_POST['apply_for_workcrew']))?1:0;
			$_POST['apply_for_staff'] = (isset($_POST['apply_for_staff']))?1:0;
			# Sanity Checks

			# Work crew sanity checks
			$workcrew_eligible = $this->_get_workcrew_eligible($_SESSION['APPLICATION_INDIVIDUAL_id']);
			if($workcrew_eligible != 1 && $_POST['apply_for_workcrew'] == 1){
				$_POST['apply_for_workcrew'] = 0;
				$data['error_message'] = $_POST['first_name'].' '.$_POST['last_name'].' is not eligible for workcrew';
				$redirect = FALSE;
			}

			# Save the POST variables by and API call
			//$response = $this->_save_POST_via_api();

			# Check to see if we need to change session variables
			if($_SESSION['APPLICATION_INDIVIDUAL_id'] == $_SESSION['INDIVIDUAL_id']){
				$_SESSION['first_name'] = $_POST['first_name'];
				$_SESSION['last_name'] = $_POST['last_name'];
			}
			$_SESSION['application_full_name'] = $_POST['first_name'].' '.$_POST['last_name'];
			# We have saved.  Let's continue!
			if($redirect){
				if($_POST['apply_for_workcrew']==1){
					redirect('form/medical');
				} else {
					redirect('form/contact-information');
				}
			}
		}

		# Check if they were redirected with an error message
		$error_message = $this->session->flashdata('error_message');
		if($error_message){
			$data['error_message'] = $error_message;
		}

		# Pull the application details from the API into the view
		$data['application_details'] = $app = $this->_get_application_details();
		# Pass other variables into the view
		$data['application_id'] = $_SESSION['APPLICATION_id'];
		$data['percent_complete'] = $percent_complete = $this->_get_percent_complete($_SESSION['APPLICATION_id']);
		$this->_participation_agreement_check($percent_complete);
		$data['workcrew_eligible'] = $this->_get_workcrew_eligible($app->INDIVIDUAL_id);
		$data['body_class'] = 'preliminary_questions';
		$data['title'] = 'Preliminary Questions';

		# Load the views
		$data['sidebar'] = $this->load->view('form/sidebar', $data, TRUE);
		$data['application'] = $this->load->view('form/preliminary_questions', $data, TRUE);
		$this->load->view('template', $data);
	}


	/**
	* Preselection medical questions form page
	*
	* @access public
	* @return none
	*/
	public function medical(){
		# Check to see if the user is signed in
		$this->_sign_in_check();
		# Check for a valid application id
		$this->_application_id_check();

		# Remove unneeded session data
		if(isset($_SESSION['redirect_after_crop'])){unset($_SESSION['redirect_after_crop']);}

		# Pull the application details from the API into the view
		$data['application_details'] = $app = $this->_get_application_details();

		# Check to see if there is a form POST
		$save_and_continue = $this->input->post('save_and_continue');
		if($save_and_continue){
			if((int)$app->apply_for_workcrew == 1){
				# We need to change the checkboxes
				$_POST['has_add_or_adhd'] = (isset($_POST['has_add_or_adhd']))?1:0;
				$_POST['has_aspergers_syndrome'] = (isset($_POST['has_aspergers_syndrome']))?1:0;
				$_POST['has_asthma'] = (isset($_POST['has_asthma']))?1:0;
				$_POST['has_bleeding_disorder'] = (isset($_POST['has_bleeding_disorder']))?1:0;
				$_POST['has_blood_borne_disease'] = (isset($_POST['has_blood_borne_disease']))?1:0;
				$_POST['has_clotting_disorder'] = (isset($_POST['has_clotting_disorder']))?1:0;
				$_POST['has_diabetes'] = (isset($_POST['has_diabetes']))?1:0;
				$_POST['has_emotional_personality_disorder'] = (isset($_POST['has_emotional_personality_disorder']))?1:0;
				$_POST['has_epilepsy_seizure_disorder'] = (isset($_POST['has_epilepsy_seizure_disorder']))?1:0;
				$_POST['has_heart_problems_arrhythmia'] = (isset($_POST['has_heart_problems_arrhythmia']))?1:0;
				$_POST['has_heart_illness'] = (isset($_POST['has_heart_illness']))?1:0;
				$_POST['has_high_blood_pressure'] = (isset($_POST['has_high_blood_pressure']))?1:0;
				$_POST['has_gi_disorder_crohns_uc'] = (isset($_POST['has_gi_disorder_crohns_uc']))?1:0;
				$_POST['has_kidney_problems'] = (isset($_POST['has_kidney_problems']))?1:0;
				$_POST['has_lung_problems'] = (isset($_POST['has_lung_problems']))?1:0;
				$_POST['has_marfan_syndrome'] = (isset($_POST['has_marfan_syndrome']))?1:0;
				$_POST['has_musculoskeletal'] = (isset($_POST['has_musculoskeletal']))?1:0;
				$_POST['has_sickle_cell_disease'] = (isset($_POST['has_sickle_cell_disease']))?1:0;
				$_POST['has_sleeping_disorder'] = (isset($_POST['has_sleeping_disorder']))?1:0;
				$_POST['has_stroke'] = (isset($_POST['has_stroke']))?1:0;
				$_POST['has_autism'] = (isset($_POST['has_autism']))?1:0;
				//$_POST['test'] = TRUE;
			}
			# Save the POST variables by and API call
			$response = $this->_save_POST_via_api();
			# We have saved.  Let's continue!
			redirect('form/contact-information');

		}

		# Pass other variables into the view
		$data['application_id'] = $_SESSION['APPLICATION_id'];
		$data['percent_complete'] = $percent_complete = $this->_get_percent_complete($_SESSION['APPLICATION_id']);
		$this->_participation_agreement_check($percent_complete);
		$data['body_class'] = 'medical';
		$data['title'] = 'Medical Questions';

		# Load the views
		$data['sidebar'] = $this->load->view('form/sidebar', $data, TRUE);
		$data['application'] = $this->load->view('form/medical', $data, TRUE);
		$this->load->view('template', $data);
	}


	/**
	* Gather's personal experience
	*
	* @access public
	* @return null
	*/
	public function review(){
		# Check to see if the user is signed in
		$this->_sign_in_check();
		$this->_application_id_check();

		# Get data
		$data['application_details'] = $application_details = $this->_get_application_details();

		# Check to see if there is a form POST
		$mark_as_reviewed = $this->input->post('mark_as_reviewed');
		if($mark_as_reviewed){
			# Change the status by an API call
			if($application_details->status != "Selected"){
				$api_data['api_credentials'] = $api_credentials = $this->_get_api_credentials();
				$api_data['status'] = 'Reviewed';
				$response = Requests::post($this->config->item('api_url').'v2/application/'.$_SESSION['APPLICATION_id'], array(), $api_data);
				# We have updated the status.  Now update the application history.
				$history = array('api_credentials'=>$api_credentials, 'APPLICATION_id'=>$_SESSION['APPLICATION_id'], 'action'=>'Application reviewed', 'action_taken_by'=>$_SESSION['INDIVIDUAL_id'],'notes'=>'Application reviewed by '.$_SESSION['first_name'].' '.$_SESSION['last_name']);
				$response = Requests::post($this->config->item('api_url').'v2/application-history/', array(), $history);
			}
			# Redirect to the dashboard.
			redirect('dashboard');
		}

		# Get data and send it to the view
		$data['application_id'] = $_SESSION['APPLICATION_id'];
		$data['percent_complete'] = $percent_complete = $this->_get_percent_complete($_SESSION['APPLICATION_id']);
		$this->_participation_agreement_check($percent_complete);
		$data['past_pageants'] = $this->_get_past_pageants();
		$data['applying_for'] = applying_for($application_details);
		$data['ecclesiastical_leader_details'] = ($application_details)? $this->_get_ecclesiastical_leader_details($application_details->ECCLESIASTICAL_LEADER_id) : FALSE;
		$data['body_class'] = 'review';
		$data['title'] = 'Review';
		$data['sidebar'] = $this->load->view('form/sidebar', $data, TRUE);
		$data['application'] = $this->load->view('form/review', $data, TRUE);
		$this->load->view('template', $data);
	}


	/**
	* This will start the application process and redirect to the dashboard
	*
	* @access public
	* @return none
	*/
	public function start(){
		# Check to see if the user is signed in
		$this->_sign_in_check();

		$data['api_credentials'] = $api_credentials = $this->_get_api_credentials();

		# Get the latest household record
		$response = Requests::get($this->config->item('api_url').'v2/latest-household-for-individual/'.$_SESSION['INDIVIDUAL_id']);
		$household = json_decode($response->body)->data->household;

		# Get the current pageant year
		$data['pageant_year'] = $this->_get_current_application_year();
		$data['status'] = 'Started';
		$data['head_of_household'] = 1;
		$data['apply_for_cast'] = 1;
		$data['INDIVIDUAL_id'] = $_SESSION['INDIVIDUAL_id'];

		# Get the largest household id
		$response = Requests::get($this->config->item('api_url').'v2/household/max');
		$data['HOUSEHOLD_id'] = ((int) json_decode($response->body)->data->HOUSEHOLD_id) + 1;

		# Create a new application record
		$response = Requests::post($this->config->item('api_url').'v2/application/', array(), $data);
		$app = $this->_handle_api_response($response);

		# Log the action
		$history = array('APPLICATION_id'=>$app->id, 'api_credentials'=>$api_credentials, 'action'=>'Application started', 'action_taken_by'=>$_SESSION['INDIVIDUAL_id'], 'notes'=>'Application started by '.$_SESSION['first_name'].' '.$_SESSION['last_name']);
		$response = Requests::post($this->config->item('api_url').'v2/application-history/', array(), $history);

		# Set the two session variables needed for the application process
		$_SESSION['APPLICATION_id'] = $app->id;
		$_SESSION['APPLICATION_INDVIDUAL_id'] = $_SESSION['INDIVIDUAL_id'];

		# Redirect to the dashboard
		redirect('dashboard');
	}

	/**
	* This will start the application process for a new individual
	*
	* @access public
	* @return none
	*/
	public function start_from_scratch(){
		# Check to see if the user is signed in
		$this->_sign_in_check();
		$this->_application_id_check();

		# Get the api credentails
		$individual_data['api_credentials'] = $data['api_credentials'] = $this->_get_api_credentials();
		# In order to create an individaul record we need to post some bogus data about the individual
		$individual_data['single'] = 1;
		# Create the individual record
		$response = Requests::post($this->config->item('api_url').'v2/individual/', array(), $individual_data);
		$individual = $this->_handle_api_response($response);
		# Now clear out the bogus data
		$individual_data['single'] = null;
		$response = Requests::post($this->config->item('api_url').'v2/individual/', array(), $individual_data);

		# Get the current pageant year
		$data['pageant_year'] = $this->_get_current_application_year();
		# Start the application data
		$data['status'] = 'Started';
		$data['apply_for_cast'] = 1;
		$data['INDIVIDUAL_id'] = $individual->id;
		$data['HOUSEHOLD_id'] = $this->input->post('HOUSEHOLD_id');

		# Create a new application record
		$response = Requests::post($this->config->item('api_url').'v2/application/', array(), $data);
		$app = $this->_handle_api_response($response);

		# Redirect to the preliminary questions screen
		# The user wants to edit so set the session variables and redirect
		$_SESSION['APPLICATION_id'] = $app->id;
		$_SESSION['APPLICATION_INDIVIDUAL_id'] = $individual->id;
		$_SESSION['application_full_name'] = 'New Individual';
		redirect('form/participation-agreement');
	}

	public function work_crew_instructions(){
		$this->load->view('form/work_crew_instructions');
	}

}
