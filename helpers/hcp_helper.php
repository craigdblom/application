<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Generalized function to return an API response
*
* This function takes the response object from PHP Requests and checks if data
* exists.  If it does it will return it.  If it doesn't it returns FALSE
*
* @param object $response
* @return mixed
*/
function handle_api_response($response){
	if($response->status_code == 200){
		# Valid API response so decode JSON object
		$json = json_decode($response->body);
		# if data exists return it
		if(isset($json->data)){
			return($json->data);
		}
	}
	# Request failed so return FALSE
	return(FALSE);
}

/**
* Used CURL to make call to the API
* -=[ Hat tip to https://davidwalsh.name/curl-post ]=-
*
* @param string $url The URL of the API endpoint
* @param mixed $post_data and array that should be posted.  FALSE if nothing is
* passed in.
* @return object
*/
function age_at_pageant($hcp_start_date=FALSE, $date_of_birth=FALSE, $difference_format = '%y'){
  if(!$hcp_start_date || !$date_of_birth){return(FALSE);}
  $datetime1 = date_create($hcp_start_date);
  $datetime2 = date_create($date_of_birth);
  $interval = date_diff($datetime1, $datetime2);
  return $interval->format($difference_format);
}

/**
* Used CURL to make call to the API
* -=[ Hat tip to https://davidwalsh.name/curl-post ]=-
*
* @param string $url The URL of the API endpoint
* @param mixed $post_data and array that should be posted.  FALSE if nothing is
* passed in.
* @return object
*/
function api_call($url, $post_data = FALSE){
  # Open connection
  $ch = curl_init();
  # Set the URL
  curl_setopt($ch,CURLOPT_URL, $url);
  # Check for POST vars
  if($post_data){
    $fields_string = '';
    # Set the number of POST vars, POST data and URL-ify the data for the POST
    foreach($post_data as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');
    # POST the data
    curl_setopt($ch,CURLOPT_POST, count($post_data));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
  }
  # Execute CURL call
  $response_json = curl_exec($ch);
  # Close connection
  curl_close($ch);
  # Read the JSON response and return it
  $response = json_decode($response_json, TRUE);
  return($response);
}


/**
* Generalized function to translate a boolean flag to yes or no
*
* @param integer $var The variable to evaluate
* @return string
*/
function yes_no($var, $return_no_on_null = TRUE){
  if($var == 1){
    return('Yes');
  }
  elseif($var == 0){
    return('No');
  }
  elseif ($var == '') {
    if($return_no_on_null){
      return('No');
    } else {
      return('');
    }
  }
  return('Unknown');
}


/**
* Generalized function to report on the status of ecclesiastical endorsements
*
* @param string $status the status of the HCP application
* @param mixed $check_if_null the variable we need to check if it is null
* @return string
*/
function get_status($status, $check_if_null){
  if($status != 'Submitted') {
    return('Not Requested');
  } elseif($check_if_null == null) {
    return('Requested but not received');
  }
  return('Received');
}


/**
* Get's the status of the endorsement
*
* @param object $application_details the HCP application
* @return string
*/
function endorsement_status($application_details){
  return get_status($application_details->status, $application_details->ecclesiastical_endorsement_received);
}


/**
* Get's the workcrew letter_of recommendation status
*
* @param object $application_details the HCP application
* @return string
*/
function workcrew_letter_of_recommendation_status($application_details){
  return get_status($application_details->status, $application_details->workcrew_letter_of_recommendation);
}


/**
* Used to determine what SVG should be displayed
*
* @param object $percent_complete The object with the percent complete data
* @param string $step The step of the application
* @param boolean $preselection TRUE (default) if this is for the preselection questions
* @return string
*/
function sidebar_indicator($percent_complete, $step, $preselection = TRUE){
  if($preselection){
    $percent_complete = $percent_complete->preselection;
  } else {
    $percent_complete = $percent_complete->postselection;
  }

  if($percent_complete->$step < 1){
    return('not_completed');
  } else {
    return('completed');
  }
}


function applying_for($app){
  $return = '';
  if($app->apply_for_cast == "1"){$return .= 'Cast, ';}
  if($app->apply_for_staff == "1"){$return .= 'Staff, ';}
  if($app->apply_for_workcrew == "1"){$return .= 'Work Crew, ';}
  $return = rtrim($return, ', ');

  if (strlen($return)>0){
    return($return);
  } else {
    return 'N/A';
  }
}
