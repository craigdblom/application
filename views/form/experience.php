<?php echo $sidebar;?>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=gyy3osr5lwvsomazx1ocyp85bdqljl0k98zmnn7h2227zrj6"></script>
<article class="card nine columns" id="application_form">
  <h2>Experience</h2>
  <?php if(isset($error_message)):?>
    <div id="error_message"><?php echo $error_message;?></div>
  <?php endif;?>
  <form action="<?php echo current_url();?>" method="post">
    <section>
      <h3>Tell us about your skills</h3>

      <div class="cf">
        <div class="span4"><a name="theater"></a>
          <label>Do you have theater experience?</label>
          <?php echo form_dropdown('qual_theater', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->qual_theater, 'class="autosave qualifications_follow_up" id="qual_theater" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </div>
        <div class="span4 col"><a name="music"></a>
          <label>Do you have musical talents?</label>
          <?php echo form_dropdown('qual_music', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->qual_music, 'class="autosave qualifications_follow_up" id="qual_music" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </div>
        <div class="span4 col"><a name="it"></a>
          <label>Do you have IT experience?</label>
          <?php echo form_dropdown('qual_it', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->qual_it, 'class="autosave qualifications_follow_up" id="qual_it" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </div>
      </div>

      <div class="cf<?php if(!$application_details->qual_theater && !$application_details->qual_music && !$application_details->qual_it):?> hidden<?php endif;?>" id="qualifications_container">
        <h3>You answered yes above.  Please elaborate in the space below:</h3><a name="qualifications"></a>
        <textarea class="autosave_html" id="qualifications" name="qualifications" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>"><?php echo $application_details->qualifications;?></textarea>
      </div>

      <div class="cf"><a name="church_callings"></a>
        <label>Please list previous and current church callings?</label>
        <textarea class="autosave_html" id="church_callings" name="church_callings" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>"><?php echo $application_details->church_callings;?></textarea>
      </div>

      <div class="cf"><a name="health"></a>
        <div id="health_professional_container" class="span<?php echo ($application_details->health_professional==0)?'12':'4';?>">
          <label>Are you a health care professional or do you work in an allied health field?</label>
          <?php echo form_dropdown('health_professional', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->health_professional, 'class="autosave" id="health_professional" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </div>
        <div id="profession_container" class="<?php echo ($application_details->health_professional==0)?'hidden':'span4 col';?>">
          <label>Profession</label>
          <input class="autosave" type="text" id="profession" name="profession" value="<?php echo $application_details->profession;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
        <div id="state_of_licensure_container" class="<?php echo ($application_details->health_professional==0)?'hidden':'span4 col';?>">
          <label>State of licensure</label>
          <input class="autosave" type="text" id="state_of_licensure" name="state_of_licensure" value="<?php echo $application_details->state_of_licensure;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
      </div>

      <div class="cf"><a name="foreign-language"></a>
        <div id="qual_languages_container" class="span<?php echo ($application_details->qual_languages==0)?'12':'6';?>">
            <label>Do you speak a foreign language?</label>
            <?php echo form_dropdown('qual_languages', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->qual_languages, 'class="autosave" id="qual_languages" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </div>
        <div id="other_languages_container" class="<?php echo ($application_details->qual_languages==0)?'hidden':'span6 col';?>">
          <label>What foreign language(s)?</label>
          <input id="other_languages" class="autosave" type="text" name="other_languages" value="<?php echo $application_details->other_languages;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
      </div>
    </section>

    <section>
      <h3>Please write a personal letter of recommendation  (<?php echo ($application_details->apply_for_workcrew == 1)?'required':'optional';?>)</h3><a name="letter-of-recommendation"></a>
      <?php if($application_details->apply_for_workcrew == 1):?><p><b>Instructions:</b> Please describe Why you should be considered for the work crew.  Include: any skills and/or experiences that you feel would be particularly helpful to your participation on the work crew; Employment, service and leadership experiences you have had; and Testimony Building Experiences you have had.</p><?php endif;?>
      <textarea class="autosave_html" id="comments" name="comments" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>"><?php echo $application_details->comments;?></textarea>
    </section>

    <section class="cf">
      <h3>Previous Pageants</h3>
      <p>Have you applied for or participated in the Hill Cumorah Pageant before?  If so please indicate when (what year) below:</p>
      <span class="span4">
        <input type="number" id="year_to_add" name="year_to_add"/>
        <a class="green_button button" id="add_pageant_year">Add Pageant Year</a>
      </span>
      <span class="span8 col" id="past_pageant_container"><?php if($past_pageants): foreach ($past_pageants as $key => $past_pageant):?><a class="pill remove_pageant_year" data-year="<?php echo $past_pageant;?>" id="past_pageant_<?php echo $past_pageant;?>"><?php echo $past_pageant;?></a><?php endforeach; endif;?>
      </span>
    </section>

    <input type="submit" name="save_and_continue" value="Save and Continue"/><br/>
    <a class="button" href="<?php echo base_url();?>form/ecclesiastical-leader">Previous Page</a>

  </form>
</article>
<script>
var ACCOUNT_ID = <?php echo $_SESSION['ACCOUNT_id']?>;
var TOKEN = '<?php echo $_SESSION['token'];?>';
var PAST_YEARS = <?php echo ($past_pageants)?json_encode($past_pageants):'[]';?>;

function strip_returns(str){
    str = str.replace(/(?:\r\n|\r|\n)/g, '');
    return(str);
}

function strip_double_quotes(vars){
  if(typeof vars === 'string' || vars instanceof String){
    return vars.replace(/"/g, '&quot;');
  } else {
    return vars;
  }
}

function update_percent_complete(){
  var url = '<?php echo $this->config->item('api_url');?>v2/application/<?php echo $_SESSION['APPLICATION_id'];?>/percent-complete';
  var data = jQuery.parseJSON('{"api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    dataType: 'json',
    success: function(response){
      percent = Math.round(response.data.preselection.total*100);
      animate();
    }
  });
};

function start_save_bar(){
  $('#savebar').addClass('saving');
  $('#savebar').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
    $('#savebar').removeClass('saving');
  });
}

function autosave(name, val, url){
  val = strip_double_quotes(val);
  start_save_bar();

  var data = jQuery.parseJSON('{"'+name+'": "'+val+'", "api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');

  $.ajax({
    type: "POST",
    url: url,
    data: data,
    dataType: 'json',
    success: function(data){
      // Sucess
      $('[name="'+name+'"]').removeClass('has_error');
      $('#'+name+'_error').remove();
      update_percent_complete();
    },
    error: function(data){
      // Failure to save data
      response = data.responseJSON;
      data = response.data;
      $('#savebar').removeClass('saving').addClass('saved');
      $('[name="'+name+'"]').addClass('has_error');
      error_id = name+'_error';
      if($("#"+error_id).length == 0){
        $('<span id="'+error_id+'" class="error_message">'+data.error_message+'</span>').insertAfter('[name="'+name+'"]');
      }
    }
  });
};


$('.autosave').change(function(){
  name = $(this).attr('name');
  val = $(this).val();
  autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
  autosave(name, val, autosave_url);
});

$('.qualifications_follow_up').change(function(){
  var qual_theater = $('#qual_theater').val();
  var qual_leadership = $('#qual_leadership').val();
  var qual_music = $('#qual_music').val();
  var qual_it = $('#qual_it').val();
  if(qual_theater == 1 || qual_leadership == 1 || qual_music == 1 || qual_it == 1){
    $('#qualifications_container').removeClass('hidden');
  } else {
    $('#qualifications_container').addClass('hidden');
  }
});

$('#health_professional').change(function(){
  var i = $(this).val();
  if(i == 1){
    $('#health_professional_container').removeClass('span12').addClass('span4');
    $('#profession_container').removeClass('hidden').addClass('span4 col');
    $('#state_of_licensure_container').removeClass('hidden').addClass('span4 col');
  } else {
    $('#health_professional_container').addClass('span12').removeClass('span4');
    $('#profession_container').addClass('hidden').removeClass('span4 col');
    $('#state_of_licensure_container').addClass('hidden').removeClass('span4 col');
    // Clear out form values
    $('#profession').val('');
    $('#state_of_licensure').val();
    // Save via API calls
    autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$('#profession').attr('data-endpoint');
    autosave('profession', '', autosave_url);
    autosave('state_of_licensure', '', autosave_url);
  }
});

$('#qual_languages').change(function(){
  var i = $(this).val();
  if(i == 1){
    $('#qual_languages_container').removeClass('span12').addClass('span6');
    $('#other_languages_container').removeClass('hidden').addClass('span6 col');
    $('#other_languages').focus();
  } else {
    $('#qual_languages_container').addClass('span12').removeClass('span6');
    $('#other_languages_container').addClass('hidden').removeClass('span6 col');
    $('#other_languages').val('');
    // Save via API calls
    autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$('#other_languages').attr('data-endpoint');
    autosave('other_languages', '', autosave_url);
  }
});

$('#add_pageant_year').click(function(){
  var year = Number($('#year_to_add').val());
  var current_year = <?php echo $_SESSION['current_application_year'];?>;
  var error_id = 'year_to_add_error';
  if(year < current_year && year > (current_year - 100) ){
    // Remove errors if they exist
    $('#year_to_add').removeClass('has_error');
    if($("#"+error_id).length == 0){
      $("#"+error_id).remove();
    }
    // Save the year
    if(!PAST_YEARS.includes(year)){
      start_save_bar();
      // Save the data
      var data = jQuery.parseJSON('{"action": "add", "INDIVIDUAL_id":"<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>", "pageant_year": "'+year+'", "api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
      var name = 'add_pageant_year';
      $.ajax({
        type: "POST",
        url: "<?php echo $this->config->item('api_url');?>v2/historic_application",
        data: data,
        dataType: 'json',
        success: function(data){
          // Sucess
          PAST_YEARS.push(year);
          var a = $('<a></a>');
          a.html(year).attr('data-year', year).attr('id','past_pageant_'+year).addClass('pill').addClass('remove_pageant_year');
          $('#past_pageant_container').append(a);

          $('#year_to_add').removeClass('has_error');
          $('#year_to_add_error').remove();
        },
        error: function(data){
          // Failure to save data
          response = data.responseJSON;
          data = response.data;
          $('#savebar').removeClass('saving').addClass('saved');
          $('#year_to_add').addClass('has_error');
          error_id = 'year_to_add_error';
          if($("#"+error_id).length == 0){
            $('<span id="'+error_id+'" class="error_message">'+data.error_message+'</span>').insertAfter('[name="'+name+'"]');
          }
        }
      });
    }
  } else {
    // There's an error
    $('#year_to_add').addClass('has_error');
    if($("#"+error_id).length == 0){
      $('<span id="'+error_id+'" class="error_message">Invalid Year</span>').insertAfter('[name="year_to_add"]');
    }
  }
});


$('body').on('click', '.remove_pageant_year', function(){
  var year = $(this).attr('data-year');
  var data = jQuery.parseJSON('{"action": "remove", "INDIVIDUAL_id":"<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>", "pageant_year": "'+year+'", "api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');

  start_save_bar();

  $.ajax({
    type: "POST",
    url: "<?php echo $this->config->item('api_url');?>v2/historic_application",
    data: data,
    dataType: 'json'
  }).done(function() {
    PAST_YEARS = jQuery.grep(PAST_YEARS, function(value) {
      return value != year;
    });
    $('#past_pageant_'+year).remove();
  });
});

tinymce.init({
  selector: 'textarea.autosave_html',
  height: 300,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help'
  ],
  toolbar: 'undo redo |  bold italic | bullist numlist | removeformat',
  setup:function(ed) {
    ed.on('blur', function(e) {
      name = ed.id;
      val = strip_returns(ed.getContent());
      endpoint = $('#'+name).attr('data-endpoint');
      autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+endpoint;
      autosave(name, val, autosave_url);
    });
  }
});
</script>
