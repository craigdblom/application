<?php echo $sidebar;?>
<?php
if($application_details->has_add_or_adhd ||
$application_details->has_aspergers_syndrome ||
$application_details->has_asthma ||
$application_details->has_autism ||
$application_details->has_bleeding_disorder ||
$application_details->has_blood_borne_disease ||
$application_details->has_clotting_disorder ||
$application_details->has_brace_or_supportive_device ||
$application_details->has_diabetes ||
$application_details->has_emotional_personality_disorder ||
$application_details->has_epilepsy_seizure_disorder ||
$application_details->has_heart_problems_arrhythmia ||
$application_details->has_heart_illness ||
$application_details->has_high_blood_pressure ||
$application_details->has_gi_disorder_crohns_uc ||
$application_details->has_scheduled_surgeries ||
$application_details->has_kidney_problems ||
$application_details->has_lung_problems ||
$application_details->has_marfan_syndrome ||
$application_details->has_musculoskeletal ||
$application_details->has_sickle_cell_disease ||
$application_details->has_sleeping_disorder ||
$application_details->has_stroke){
    $need_diagnose_details = TRUE;
} else {
    $need_diagnose_details = FALSE;
}
?>
<article class="card nine columns" id="application_form">
  <h2>Medical</h2>
  <?php if(isset($error_message)):?>
    <div id="error_message"><?php echo $error_message;?></div>
  <?php endif;?>
  <form action="<?php echo current_url();?>" method="post">
    <?php /*if(($application_details->apply_for_staff + $application_details->apply_for_cast + $application_details->apply_for_workcrew) == 0):?>
      <p>You must specify how you want to participate in the pageant first</p>
    <?php endif;?>
    <?php if($application_details->apply_for_cast == 1 || $application_details->apply_for_staff == 1):?>
      <section class="cf">
        <h3>The Hill Cumorah Pageant is physically demanding.  Can you:</h3>
        <div class=" cf">
          <span class="span4">
            <label>Go up a steep (10%) incline?</label><a name="go-up-incline"></a>
            <?php echo form_dropdown('can_go_up_incline', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->can_go_up_incline, 'class="autosave" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
          </span>
          <span class="span4 col">
            <label>Navigate in the dark?</label><a name="navigate-in-dark"></a>
            <?php echo form_dropdown('can_navigate_in_dark', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->can_navigate_in_dark, 'class="autosave" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
          </span>
          <span class="span4 col">
            <label>Kneel and get up without assistance?</label><a name="kneel-and-get-up"></a>
            <?php echo form_dropdown('can_kneel_and_get_up', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->can_kneel_and_get_up, 'class="autosave" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
          </span>
        </div>
      </section>
    <?php endif;*/?>
    <?php if($application_details->apply_for_workcrew == 1):?>
      <section class="cf">
        <h3>Work Crew Application – Personal Medical History and Physical Form</h3>
        <p>Participation on Work Crew of the Hill Cumorah Pageant will bring untold spiritual blessings to you and our guests. You will benefit most and serve best if you are in excellent condition. Participation on the Work Crew may pose hazards to your health if you are in poor physical condition or have compromising physical or emotional conditions, allergy problems or difficulty walking, running, hearing or seeing. Participants must be ready to engage in emotionally and physically stressful and strenuous activities under conditions of sun exposure, high heat, high humidity, and exposure to insects and poison ivy.</p>
        <p>It is imperative that the Pageant health personnel be aware of participants who have compromising health conditions that may require special considerations. A letter of medical recommendation may be required as a condition for participation in Pageant Work Crew.</p>
        <h3>Personal Information</h3>
        <div class=" cf">
          <span class="span4">
            <div class="cf">
              <span class="span6">
                <label>Height: Feet</label>
                <input class="autosave" type="number" id="height_feet" name="height_feet" value="<?php echo $application_details->height_feet;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
              </span>
              <span class="span6">
                <label>Inches</label>
                <input class="autosave" type="number" id="height_inches" name="height_inches" value="<?php echo $application_details->height_inches;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
              </span>
            </div>
          </span>
          <span class="span4 col">
            <label>Weight (in lbs)</label>
            <input class="autosave" type="number" name="weight_lbs" value="<?php echo $application_details->weight_lbs;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
          </span>
          <span class="span4 col">
            <label>Year of Last Tetanus Shot</label><a name="tetanus"></a>
            <input class="autosave" type="number" name="tetanus_immunization_year" value="<?php echo $application_details->tetanus_immunization_year;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
          </span>
        </span>
      </div>
    </section>

    <section>
      <h3>Allergies</h3><a name="allergies"></a>
      <p>Have you ever had an allergic reaction or side effect to:</p>
      <div class="cf">
        <div class="span6">
          <label>Drugs?</label>
          <?php echo form_dropdown('drug_allergy', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->drug_allergy, 'class="autosave allergy_followup" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </div>
        <div class="span6 col">
          <label>Food?</label>
          <?php echo form_dropdown('food_allergy', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->food_allergy, 'class="autosave allergy_followup" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </div>
      </div>
      <div class="cf">
        <div class="span6">
          <label>Plants?</label>
          <?php echo form_dropdown('plant_allergy', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->plant_allergy, 'class="autosave allergy_followup" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </div>
        <div class="span6 col">
          <label>Insect Stings?</label>
          <?php echo form_dropdown('insect_sting_allergy', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->insect_sting_allergy, 'class="autosave allergy_followup" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </div>
      </div>
      <div class="cf<?php if(!$application_details->drug_allergy && !$application_details->food_allergy && !$application_details->plant_allergy && !$application_details->insect_sting_allergy):?> hidden<?php endif;?>" id="allergy_container">
        <p>You answered yes above.  Please elaborate in the space below:</p>
        <?php /*<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=gyy3osr5lwvsomazx1ocyp85bdqljl0k98zmnn7h2227zrj6"></script>*/?>
        <textarea class="autosave" id="allergy_note" name="allergy_note" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>"><?php echo $application_details->allergy_note;?></textarea>
      </div>
    </section>

    <section>
      <h3>Medical History</h3>
      <div class="cf">
        <div class="span6">
          <label>Do you regularly take any prescription medications<!--, non-prescription medications, supplements, vitamins, herbal remedies-->?</label><a name="medications"></a>
          <?php echo form_dropdown('medications', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->medications, 'class="autosave followup" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
          <div class="<?php if($application_details->medications == 0){echo' hidden';}?>" id="medications_followup"><a name="medications-followup"></a>
            <p>Please explain:</p>
            <textarea class="autosave" name="medications_list" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>"><?php echo $application_details->medications_list;?></textarea>
          </div>
        </div>
        <div class="span6 col">
          <label>Do you have any difficulty walking, running, seeing, or hearing?</label><a name="run-walk-see"></a>
          <?php echo form_dropdown('difficulty_running_walking_seeing_hearing', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->difficulty_running_walking_seeing_hearing, 'class="autosave followup" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
          <div class="<?php if($application_details->difficulty_running_walking_seeing_hearing == 0){echo' hidden';}?>" id="difficulty_running_walking_seeing_hearing_followup"><a name="run-walk-see-followup"></a>
            <p>Please explain</p>
            <textarea class="autosave" name="difficulty_note" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>"><?php echo $application_details->difficulty_note;?></textarea>
          </div>
        </div>
      </div>
      <div class="cf">
        <div class="span6">
          <label>Do you require a special diet?</label><a name="special-diet"></a>
          <?php echo form_dropdown('special_diet', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->special_diet, 'class="autosave followup" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
          <div class="<?php if($application_details->special_diet == 0){echo' hidden';}?>" id="special_diet_followup" >
            <p>Please explain:</p><a name="special-diet-followup"></a>
            <textarea class="autosave" name="special_diet_note" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>"><?php echo $application_details->special_diet_note;?></textarea>
          </div>
        </div>
        <div class="span6 col">
          <label>Do you have any other special health needs?</label><a href="special-needs"></a>
          <?php echo form_dropdown('other_special_health_needs', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->other_special_health_needs, 'class="autosave followup" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
          <div class="<?php if($application_details->other_special_health_needs == 0){echo' hidden';}?>" id="other_special_health_needs_followup">
            <p>Please explain:</p><a name="special-needs-followup"></a>
            <textarea class="autosave" name="other_special_health_needs_note" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>"><?php echo $application_details->other_special_health_needs_note;?></textarea>
          </div>
        </div>
      </div>
    </section>

    <section class="cf"><a name="medical-conditions"></a>
      <p>Have you ever been diagnosed with: (Please check any/all that apply and explain below)</p>
      <div class="span4">
        <p><input id="has_add_or_adhd" class="checkbox has" type="checkbox" name="has_add_or_adhd" value="1"<?php if($application_details->has_add_or_adhd){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_add_or_adhd"> ADD/ADHD</label></p>
        <p><input id="has_aspergers_syndrome" class="checkbox has" type="checkbox" name="has_aspergers_syndrome" value="1"<?php if($application_details->has_aspergers_syndrome){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_aspergers_syndrome"> Asperger's Syndrome</label></p>
        <p><input id="has_asthma" class="checkbox has" type="checkbox" name="has_asthma" value="1"<?php if($application_details->has_asthma){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_asthma"> Asthma</label></p>
        <p><input id="has_autism" class="checkbox has" type="checkbox" name="has_autism" value="1"<?php if($application_details->has_autism){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_autism"> Autism</label></p>
        <p><input id="has_bleeding_disorder" class="checkbox has" type="checkbox" name="has_bleeding_disorder" value="1"<?php if($application_details->has_bleeding_disorder){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_bleeding_disorder"> Bleeding Disorder</label></p>
        <p><input id="has_blood_borne_disease" class="checkbox has" type="checkbox" name="has_blood_borne_disease" value="1"<?php if($application_details->has_blood_borne_disease){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_blood_borne_disease"> Blood Borne Disease</label></p>
        <p><input id="has_clotting_disorder" class="checkbox has" type="checkbox" name="has_clotting_disorder" value="1"<?php if($application_details->has_clotting_disorder){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_clotting_disorder"> Clotting Disorder</label></p>
        <p><input id="has_brace_or_supportive_device" class="checkbox has" type="checkbox" name="has_brace_or_supportive_device" value="1"<?php if($application_details->has_brace_or_supportive_device){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_brace_or_supportive_device"> Brace or Supportive Device</label></p>
      </div>

      <div class="span4">
        <p><input id="has_diabetes" class="checkbox has" type="checkbox" name="has_diabetes" value="1"<?php if($application_details->has_diabetes){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_diabetes"> Diabetes</label></p>
        <p><input id="has_emotional_personality_disorder" class="checkbox has" type="checkbox" name="has_emotional_personality_disorder" value="1"<?php if($application_details->has_emotional_personality_disorder){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_emotional_personality_disorder"> Emotional/Personality Disorder</label></p>
        <p><input id="has_epilepsy_seizure_disorder" class="checkbox has" type="checkbox" name="has_epilepsy_seizure_disorder" value="1"<?php if($application_details->has_epilepsy_seizure_disorder){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_epilepsy_seizure_disorder"> Epilepsy (Seizure Disorder)</label></p>
        <p><input id="has_heart_problems_arrhythmia" class="checkbox has" type="checkbox" name="has_heart_problems_arrhythmia" value="1"<?php if($application_details->has_heart_problems_arrhythmia){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_heart_problems_arrhythmia"> Heart Problems/Arrhythmia</label></p>
        <p><input id="has_heart_illness" class="checkbox has" type="checkbox" name="has_heart_illness" value="1"<?php if($application_details->has_heart_illness){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_heart_illness"> Heart Illness</label></p>
        <p><input id="has_high_blood_pressure" class="checkbox has" type="checkbox" name="has_high_blood_pressure" value="1"<?php if($application_details->has_high_blood_pressure){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_high_blood_pressure"> High Blood Pressure</label></p>
        <p><input id="has_gi_disorder_crohns_uc" class="checkbox has" type="checkbox" name="has_gi_disorder_crohns_uc" value="1"<?php if($application_details->has_gi_disorder_crohns_uc){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_gi_disorder_crohns_uc"> GI Disorder Crohns/UC</label></p>
        <p><input id="has_scheduled_surgeries" class="checkbox has" type="checkbox" name="has_scheduled_surgeries" value="1"<?php if($application_details->has_scheduled_surgeries){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_scheduled_surgeries"> Scheduled Surgeries Prior to the Pageant</label></p>
      </div>

      <div class="span4">
        <p><input id="has_kidney_problems" class="checkbox has" type="checkbox" name="has_kidney_problems" value="1"<?php if($application_details->has_kidney_problems){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_kidney_problems"> Kidney Problems</label></p>
        <p><input id="has_lung_problems" class="checkbox has" type="checkbox" name="has_lung_problems" value="1"<?php if($application_details->has_lung_problems){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_lung_problems"> Lung Problems</label></p>
        <p><input id="has_marfan_syndrome" class="checkbox has" type="checkbox" name="has_marfan_syndrome" value="1"<?php if($application_details->has_marfan_syndrome){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_marfan_syndrome"> Marfan Syndrome</label></p>
        <p><input id="has_musculoskeletal" class="checkbox has" type="checkbox" name="has_musculoskeletal" value="1"<?php if($application_details->has_musculoskeletal){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_musculoskeletal"> Musculoskeletal</label></p>
        <p><input id="has_sickle_cell_disease" class="checkbox has" type="checkbox" name="has_sickle_cell_disease" value="1"<?php if($application_details->has_sickle_cell_disease){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_sickle_cell_disease"> Sickle Cell Disease</label></p>
        <p><input id="has_sleeping_disorder" class="checkbox has" type="checkbox" name="has_sleeping_disorder" value="1"<?php if($application_details->has_sleeping_disorder){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_sleeping_disorder"> Sleeping Disorder</label></p>
        <p><input id="has_stroke" class="checkbox has" type="checkbox" name="has_stroke" value="1"<?php if($application_details->has_stroke){echo ' checked="checked"';}?>data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" /><label class="inline" for="has_stroke"> Stroke</label></p>
      </div>

      <div class="span12<?php if(!$need_diagnose_details){echo' hidden';}?>" id="diagnose_followup">
        <p>Please explain:</p><a name="diagnose-details"></a>
        <textarea class="autosave" name="has_notes" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>"><?php echo $application_details->has_notes;?></textarea>
      </div>
    </section>

    <section>
      <h3>Parent's Personal Information</h3>

      <div class="cf">
        <div class="span12">
          <label>Name</label>
          <input class="autosave" type="text" name="emergency_contact_name" value="<?php echo $application_details->emergency_contact_name;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
      </div>

      <div class="cf">
        <div class="span12">
          <label>Street address</label>
          <input class="autosave" type="text" name="emergency_contact_address" value="<?php echo $application_details->emergency_contact_address;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
      </div>

      <div class="cf">
        <div class="span7">
          <label>City</label>
          <input class="autosave" type="text" name="emergency_contact_city" value="<?php echo $application_details->emergency_contact_city;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
        <div class="span3 col">
          <label>State/Providence/Region</label>
          <input class="autosave" type="text" name="emergency_contact_state_or_province_region" value="<?php echo $application_details->emergency_contact_state_or_province_region;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
        <div class="span2 col">
          <label>Zip/Postal code</label>
          <input class="autosave" type="text" name="emergency_contact_zip_or_postal_code" value="<?php echo $application_details->emergency_contact_zip_or_postal_code;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
      </div>

      <div class="cf">
        <div class="span6">
          <label>Home Phone</label>
          <input class="autosave" type="text" name="emergency_contact_home_phone_number" value="<?php echo $application_details->emergency_contact_home_phone_number;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>

        <div class="span6 col">
          <label>Business/Cell Phone</label>
          <input class="autosave" type="text" name="emergency_contact_business_or_cell_phone_number" value="<?php echo $application_details->emergency_contact_business_or_cell_phone_number;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
        <input name="emergency_contact_relationship" value="Parent" type="hidden"/>
      </div>
    </section>

  <?php endif;?>
  <input type="submit" name="save_and_continue" value="Save and Continue"/><br/>
  <a class="button" href="<?php echo base_url();?>form/preliminary-questions">Previous Page</a>

</form>
</article>
<script>
var ACCOUNT_ID = <?php echo $_SESSION['ACCOUNT_id']?>;
var TOKEN = '<?php echo $_SESSION['token'];?>';

var MY_ADDRESS = {
  "address_line_1":"<?php echo $application_details->address_line_1;?>",
  "address_line_2":"<?php echo $application_details->address_line_2;?>",
  "city":"<?php echo $application_details->city;?>",
  "state":"<?php echo $application_details->state;?>",
  "zip":"<?php echo $application_details->zip;?>",
  "country":"<?php echo $application_details->country;?>"};

  function strip_double_quotes(vars){
    if(typeof vars === 'string' || vars instanceof String){
      return vars.replace(/"/g, '&quot;');
    } else {
      return vars;
    }
  }

  function update_percent_complete(){
    var url = '<?php echo $this->config->item('api_url');?>v2/application/<?php echo $_SESSION['APPLICATION_id'];?>/percent-complete';
    var data = jQuery.parseJSON('{"api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      dataType: 'json',
      success: function(response){
        percent = Math.round(response.data.preselection.total*100);
        animate();
      }
    });
  };

  function autosave(name, val, url){
    val = strip_double_quotes(val);
    $('#savebar').addClass('saving');
    $('#savebar').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
      $('#savebar').removeClass('saving');
    });
    var data = jQuery.parseJSON('{"'+name+'": "'+val+'", "api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      dataType: 'json',
      success: function(data){
        // Sucess
        $('[name="'+name+'"]').removeClass('has_error');
        $('#'+name+'_error').remove();
        update_percent_complete();
      },
      error: function(data){
        // Failure to save data
        response = data.responseJSON;
        data = response.data;
        $('#savebar').removeClass('saving').addClass('saved');
        $('[name="'+name+'"]').addClass('has-error');
        error_id = name+'_error';
        if($("#"+error_id).length == 0){
          $('<span id="'+error_id+'" class="error_message">'+data.error_message+'</span>').insertAfter('[name="'+name+'"]');
        }
      }
    });
  };


  $('.autosave').change(function(){
    name = $(this).attr('name');
    val = $(this).val();
    autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
    autosave(name, val, autosave_url);
  });

  $('.allergy_followup').change(function(){
    var hide = true;
    $('.allergy_followup').each(function(i,v){
      val = v.value;
      if(val == 1){hide = false;}
    });

    if(hide){
      $('#allergy_container').addClass('hidden');
      $('#allergy_note').focus();
    } else {
      $('#allergy_container').removeClass('hidden');
      $('$allergy_note').val('');
      autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$('#allergy_note').attr('data-endpoint');
      autosave('allergy_note', '', autosave_url);
    }
  });

  $('.followup').change(function(){
    name = $(this).attr('name');
    val = $(this).val();
    if(val == 1){
      $('#'+name+'_followup').removeClass('hidden').focus();
    } else {
      $('#'+name+'_followup').addClass('hidden').val('');
    }
  });

  $('.has').change(function(){
     var checked = $('.has:checked').length;
     if(checked > 0){
         $('#diagnose_followup').show();
     } else {
         $('#diagnose_followup').hide();
     }
  });

  $(".checkbox").click(function(){
    var is_clicked = $(this).is(':checked');
    var name = $(this).attr('name');
    if(is_clicked){
      var val = 1;
    } else {
      var val = 0;
    }
    autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
    autosave(name, val, autosave_url);
  });

  $('#copy_from_my_address').click(function(){
    jQuery.each(MY_ADDRESS, function(key, val) {
      $("#" + key).val(val);
    });
  });
  </script>
