<?php echo $sidebar;?>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=gyy3osr5lwvsomazx1ocyp85bdqljl0k98zmnn7h2227zrj6"></script>
<article class="card nine columns" id="application_form">
  <h2>Miscellaneous</h2>
  <?php if(isset($error_message)):?>
    <div id="error_message"><?php echo $error_message;?></div>
  <?php endif;?>
  <form action="<?php echo current_url();?>" method="post">
    <section>
      <?php if($application_details->apply_for_cast == 1 || $application_details->apply_for_staff == 1):?>
        <h3>What are your housing plans?</h3><a name="housing"></a>
        <?php echo form_dropdown('housing_preference', array(''=>'Select One', "Zion's Camp"=>"Zion's Camp", 'College Dorms'=>'College Dorms', 'Other'=>'Other'), $application_details->housing_preference, 'id="housing_preference" class="autosave" data-endpoint="application/'.$_SESSION['APPLICATION_id'].'"');?>
        <p><a id="housing_preference_details_toggle" href="#housing_preference_details_toggle">I don't know what is meant by this question.</a>
          <div class="hidden" id="housing_preference_details">
            <p><b>Zion's Camp</b> is camp ground adjacent to the Hill Cumorah. We usually get more requests for Zion's Camp than we have spaces. You will be notified if you are assigned to Zion's Camp at the time you receive your acceptance to be in the pageant. You must select your Camp type unit at the time of application.</p>
            <p><b>College Dorms</b> - The Hill Cumorah Pageant arranges housing at a local college.</p>
            <p><b>Other</b> - All housing options other than the college dorms or Zion's Camp. These options consider personal or rented home, hotel, other campgrounds, etc. Please select this if at the time of registration you are not sure where you may stay.</p>
          </div>
          <div id="zions_camp_followup_questions"<?php if($application_details->housing_preference != "Zion's Camp"):?> class="hidden"<?php endif;?>>
            <h3>Please tell us more</h3>
            <div class=" cf">
              <span class="span12">
                <label>Desired Campsite Type?</label><a name="go-up-incline"></a>
                <?php echo form_dropdown('desired_campsite_type', array(''=>'Select One', 'Motorhome'=>'Motorhome','Trailer'=>'Trailer', 'Pop-up Trailer'=>'Pop-up Trailer', '5th Wheel'=>'5th Wheel'), $application_details->desired_campsite_type, 'class="autosave zc" data-endpoint="application/'.$_SESSION['APPLICATION_id'].'"');?>
              </span>
            </div>
            <div class=" cf">
              <span class="span6">
                <label>Trailer Size</label>
                <?php echo form_dropdown('trailer_size', array(''=>'N/A', '< 10'=>'< 10','10-20'=>'10-20', '20+'=>'20+'), $application_details->trailer_size, 'class="autosave zc" data-endpoint="application/'.$_SESSION['APPLICATION_id'].'"');?>
              </span>
              <span class="span6 col">
                <label>Camping Equipment</label>
                <input class="autosave zc" type="text" name="camping_equipment" value="<?php echo $application_details->camping_equipment;?>" data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" />
              </span>
            </div>
            <div class=" cf">
              <span class="span6">
                <label>Number of Tents</label>
                <input class="autosave zc" type="number" name="number_of_tents" value="<?php echo $application_details->number_of_tents;?>" data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" />
              </span>
              <span class="span6 col">
                <label>Power Requirement</label>
                <input class="autosave zc" type="text" name="power_requirement" value="<?php echo $application_details->power_requirement;?>" data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" />
              </span>
            </div>
          </div>
        </section>
        <section class="cf">
          <h3>Would you be willing to speak to the cast during our devotionals, during sacrament meeting or to a cast team?</h3>
          <?php echo form_dropdown('qual_speaking', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->qual_speaking, 'class="autosave" id="qual_speaking" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>

          <div class="cf<?php if($application_details->qual_speaking != 1):?> hidden<?php endif;?>" id="speaking_topic_or_experience_container">
            <h3>You answered yes above.  What topics or experiences would you be willing to speak on:</h3><a name="speaking_topic_or_experience"></a>
            <textarea class="autosave_html" id="speaking_topic_or_experience" name="speaking_topic_or_experience" data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>"><?php echo $application_details->speaking_topic_or_experience;?></textarea>
          </div>

        </section>
      <?php endif;?>
      <?php $age_at_pageant = age_at_pageant($hcp_start_date, $application_details->date_of_birth); if($age_at_pageant == 17 || $age_at_pageant == 18):?>
        <section class="cf">
          <h3>Have you graduated from high school?</h3>
          <?php echo form_dropdown('high_school_graduate', array(''=>'Select One', '0'=>'No','1'=>'Yes'), $application_details->high_school_graduate, 'class="autosave" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </section>
      <?php endif;?>
      <?php if($application_details->single == 0 && ($application_details->apply_for_cast == 1 || $application_details->apply_for_staff == 1)):?>
        <section class="cf">
          <h3>Are you willing to serve as a cast team leader?</h3>
          <p>Check the boxes for the age groups for which you would be willing to serve as cast team leaders.  <b>Husband &amp; Wife Teams ONLY.</b></p>
          <p><a href="#cast_team_leader_details_toggle" id="cast_team_leader_details_toggle">I don't know what is meant by this question.</a>
            <div id="cast_team_leader_details" class="hidden">
              <p>The entire cast is divided into age groups called <i>CAST TEAMS</i>. The cast team is the center of activity for cast members. It is the primary source of information and instruction regarding rehearsals and other pageant activities and serves as a gathering group, support group, service group, etc. First time cast team leaders will be assigned to work with cast members who have previously served as cast team leaders.</p>
            </div>

            <p><input id="toggle_cast_teams" name="all_cast_team_leaders" type="checkbox" class="checkbox" value="1"<?php if($application_details->boys_girls_4_8){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="toggle_cast_teams">Check/Uncheck All</label></p>

            <div class="span4">
              <h3 class="center">Primary</h3>
              <p><input id="boys_girls_4_8" class="checkbox cast_team" type="checkbox" name="boys_girls_4_8" value="1"<?php if($application_details->boys_girls_4_8){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="boys_girls_4_8"> Boys &amp; Girls Ages 4-8</label></p>
              <p><input id="girls_9" class="checkbox cast_team" type="checkbox" name="girls_9" value="1"<?php if($application_details->girls_9){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="girls_9"> Girls Age 9</label></p>
              <p><input id="boys_9" class="checkbox cast_team" type="checkbox" name="boys_9" value="1"<?php if($application_details->boys_9){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="boys_9"> Boys Age 9</label></p>
              <p><input id="girls_10_11" class="checkbox cast_team" type="checkbox" name="girls_10_11" value="1"<?php if($application_details->girls_10_11){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="girls_10_11"> Girls Ages 10-11</label></p>
              <p><input id="boys_10_11" class="checkbox cast_team" type="checkbox" name="boys_10_11" value="1"<?php if($application_details->boys_10_11){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="boys_10_11"> Boys Ages 10-11</label></p>
            </div>

            <div class="span4 col">
              <h3 class="center">Youth</h3>
              <p><input id="girls_12_13" class="checkbox cast_team" type="checkbox" name="girls_12_13" value="1"<?php if($application_details->girls_12_13){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="girls_12_13"> Girls Ages 12-13</label></p>
              <p><input id="boys_12_13" class="checkbox cast_team" type="checkbox" name="boys_12_13" value="1"<?php if($application_details->boys_12_13){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="boys_12_13"> Boys Ages 12-13</label></p>
              <p><input id="youth_14_15" class="checkbox cast_team" type="checkbox" name="youth_14_15" value="1"<?php if($application_details->youth_14_15){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="youth_14_15"> Youth Ages 14-15</label></p>
              <p><input id="youth_16_17" class="checkbox cast_team" type="checkbox" name="youth_16_17" value="1"<?php if($application_details->youth_16_17){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="youth_16_17"> Youth Ages 16-17</label></p>
            </div>

            <div class="span4 col">
              <h3 class="center">Adult</h3>
              <p><input id="young_single_adult" class="checkbox cast_team" type="checkbox" name="young_single_adult" value="1"<?php if($application_details->young_single_adult){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="young_single_adult">  Young Single Adults</label></p>
              <p><input id="adult_cast_team" class="checkbox cast_team" type="checkbox" name="adult_cast_team" value="1"<?php if($application_details->adult_cast_team){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /><label class="inline" for="adult_cast_team"> Adults</label></p>
            </div>
          </section>
        <?php endif;?>
        <section>
          <h3>Do you or any of the applicants have any Special Needs?</h3>
          <?php echo form_dropdown('special_needs_toggle', array('0'=>'No','1'=>'Yes'), ((strlen($application_details->special_needs)<1)?0:1), 'id="special_needs_toggle"');?>
          <div id="special_needs_container"<?php if(strlen($application_details->special_needs) < 1){echo ' class="hidden"';}?>>
            <textarea class="autosave_html<?php if(strlen($application_details->special_needs) < 1){echo ' hidden';}?>" id="special_needs" name="special_needs" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>"><?php echo $application_details->special_needs;?></textarea>
          </div>
        </section>

        <input type="submit" name="save_and_continue" value="Save and Continue"/><br/>
        <a class="button" href="<?php echo base_url();?>form/experience">Previous Page</a>

      </form>
    </article>
    <script>
    var ACCOUNT_ID = <?php echo $_SESSION['ACCOUNT_id']?>;
    var TOKEN = '<?php echo $_SESSION['token'];?>';

    function strip_returns(str){
        str = str.replace(/(?:\r\n|\r|\n)/g, '');
        return(str);
    }

    function strip_double_quotes(vars){
      if(typeof vars === 'string' || vars instanceof String){
        return vars.replace(/"/g, '&quot;');
      } else {
        return vars;
      }
    }

    function update_percent_complete(){
      var url = '<?php echo $this->config->item('api_url');?>v2/application/<?php echo $_SESSION['APPLICATION_id'];?>/percent-complete';
      var data = jQuery.parseJSON('{"api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
      $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: 'json',
        success: function(response){
          percent = Math.round(response.data.preselection.total*100);
          animate();
        }
      });
    };

    function autosave(name, val, url){
      val = strip_double_quotes(val);
      $('#savebar').addClass('saving');
      $('#savebar').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
        $('#savebar').removeClass('saving');
      });
      var data = jQuery.parseJSON('{"'+name+'": "'+val+'", "api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
      $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: 'json',
        success: function(data){
          // Sucess
          $('[name="'+name+'"]').removeClass('has_error');
          $('#'+name+'_error').remove();
          update_percent_complete();
        },
        error: function(data){
          // Failure to save data
          response = data.responseJSON;
          data = response.data;
          $('#savebar').removeClass('saving').addClass('saved');
          $('[name="'+name+'"]').addClass('has-error');
          error_id = name+'_error';
          if($("#"+error_id).length == 0){
            $('<span id="'+error_id+'" class="error_message">'+data.error_message+'</span>').insertAfter('[name="'+name+'"]');
          }
        }
      });
    };

    $('#housing_preference').change(function(){
      val = $(this).val();
      if(val == "Zion's Camp"){
        $('#zions_camp_followup_questions').removeClass('hidden');
      } else {
        $('#zions_camp_followup_questions').addClass('hidden');
        $(".zc").each( function( index, element ){
          var name = $(this).attr('name');
          $(this).val('');
          autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
          autosave(name, '', autosave_url);
        });
      }
    });

  $('#special_needs_toggle').change(function(){
    val = $(this).val();
    if(val == 1){
      $('#special_needs_container').removeClass('hidden');
    } else {
      $('#special_needs_container').addClass('hidden');
      $('#special_needs').val('');
      autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$('#special_needs').attr('data-endpoint');
      autosave('special_needs', '', autosave_url);
    }
  });

  $('#qual_speaking').change(function(){
    val = $(this).val();
    if(val == 1){
      $('#speaking_topic_or_experience_container').removeClass('hidden');
      $('#speaking_topic_or_experience').focus();
    } else {
      $('#speaking_topic_or_experience_container').addClass('hidden');
      $('#speaking_topic_or_experience').val('');
      autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$('#speaking_topic_or_experience').attr('data-endpoint');
      autosave('speaking_topic_or_experience', '', autosave_url);
    }
  });

  $('.autosave').change(function(){
    name = $(this).attr('name');
    val = $(this).val();
    autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
    autosave(name, val, autosave_url);
  });


  $(".checkbox").click(function(){
    var is_clicked = $(this).is(':checked');
    var name = $(this).attr('name');
    if(is_clicked){
      var val = 1;
    } else {
      var val = 0;
    }
    autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
    autosave(name, val, autosave_url);
  });


  $('#toggle_cast_teams').click(function(){
    var checked = $(this).is(':checked');
    if(checked){val=1;}else{val=0;}
    $('input.cast_team').attr('checked', checked).each(function(i,v){
      var name = v.getAttribute('name');
      var autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+v.getAttribute('data-endpoint');
      autosave(name, val, autosave_url);
    });
  });


  $('#housing_preference_details_toggle').click(function(){
    $('#housing_preference_details').slideToggle();
  });


  $('#cast_team_leader_details_toggle').click(function(){
    $('#cast_team_leader_details').slideToggle();
  });

  tinymce.init({
    selector: 'textarea.autosave_html',
    height: 300,
    menubar: false,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor textcolor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table contextmenu paste code help'
    ],
    toolbar: 'undo redo |  bold italic | bullist numlist | removeformat',
    setup:function(ed) {
      ed.on('blur', function(e) {
        name = ed.id;
        val = strip_returns(ed.getContent());
        endpoint = $('#'+name).attr('data-endpoint');
        autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+endpoint;
        autosave(name, val, autosave_url);
      });
    }
  });
  </script>
