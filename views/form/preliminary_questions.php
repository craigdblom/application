<?php echo $sidebar;?>
<article class="card nine columns" id="application_form">
  <h2>Preliminary Questions</h2>
  <?php if(isset($error_message)):?>
    <div id="error_message"><?php echo $error_message;?></div>
  <?php endif;?>
  <form action="<?php echo current_url();?>" method="post" id="form">
    <section class="cf">
      <h3>What is your name?</h3><a name="full-name"></a>
      <span class="span6">
        <label>First Name</label>
        <input class="autosave" type="text" name="first_name" value="<?php echo $application_details->first_name;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
      </span>
      <span class="span6 col">
        <label>Last Name</label>
        <input class="autosave" type="text" name="last_name" value="<?php echo $application_details->last_name;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
      </span>
    </section>

    <section class="cf">
      <h3>Tell us about yourself?</h3>
      <div class=" cf">
        <span class="span4">
          <label>Birth Date (MM/DD/YYYY)</label><a name="date-of-birth"></a>
          <input class="autosave" type="date" name="date_of_birth" value="<?php echo $application_details->date_of_birth;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </span>
        <span class="span4 col">
          <label>Gender</label><a name="gender"></a>
          <?php echo form_dropdown('gender', array(''=>'Select One', 'M'=>'Male','F'=>'Female'), $application_details->gender, 'class="autosave" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </span>
        <span class="span4 col">
          <label>What's your marital status?</label><a name="marital-status"></a>
          <?php echo form_dropdown('single', array(''=>'Select One', '0'=>'Married','1'=>'Single'), $application_details->single, 'class="autosave" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
        </span>
      </div>
    </section>

    <section class="span12 col cf<?php if ($workcrew_eligible==-1 || $workcrew_eligible==0){echo ' hidden';}?>">
      <h3>How would you like to participate in the Pageant?</h3>
      <span class="span4"><a name="apply-for"></a>
        <input id="apply_for_cast" class="checkbox" type="checkbox" name="apply_for_cast" value="1"<?php if($application_details->apply_for_cast){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /> <label class="inline" for="apply_for_cast">Cast</label>
      </span>
      <span class="span4 col">
        <input id="apply_for_workcrew" class="checkbox" type="checkbox" name="apply_for_workcrew" value="1"<?php if($application_details->apply_for_workcrew){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /> <label class="inline" for="apply_for_workcrew">Work Crew</label>
        <span style="display: block;">Click the Work Crew Instructions button</span>
      </span>
      <span class="span4 col">
        <a name="workcrew-instructions"></a>
        <a id="work_crew_instructions_button" class="button" href="<?php echo base_url();?>work-crew-instructions" target="_blank">Work Crew Instructions</a>
      </span>
    </section>
    <input type="hidden" name="save_and_continue" value="Save and Continue" />
  </form>

  <section class="row">
    <?php echo form_open_multipart($this->config->item('settings_url'));?>
    <h2>Please upload an image of yourself</h2>
    <div class="cf"><a name="profile-image"></a>
      <div class="three columns">
        <div id="profile_image" class="rounded_image"><img src="<?php $image = ($application_details->profile_image_uploaded=="1")?$application_details->INDIVIDUAL_id:'no_image'; echo $this->config->item('img_path') . $image.'.jpg?'.md5(time());?>" /></div>
      </div>
      <div class="five columns">
        <p>This image will be viewed by the pageant presidency during their review of the applications.  It may also be used on your pageant badge if you are selected.  The photos must be .JPG file and will be cropped to a square.</p>
        <p><input type="file" name="profile_image" /></p>
      </div>
      <div class="three columns">
      <div class="slideshow">
        <img src="<?php echo $this->config->item('img_path');?>sample1.jpg" width="200" height="200" />
        <img src="<?php echo $this->config->item('img_path');?>sample2.jpg" width="200" height="200" />
        <img src="<?php echo $this->config->item('img_path');?>sample3.jpg" width="200" height="200" />
        <img src="<?php echo $this->config->item('img_path');?>sample4.jpg" width="200" height="200" />
        <img src="<?php echo $this->config->item('img_path');?>sample5.jpg" width="200" height="200" />
        <img src="<?php echo $this->config->item('img_path');?>sample6.jpg" width="200" height="200" />
        <img src="<?php echo $this->config->item('img_path');?>sample7.jpg" width="200" height="200" />
      </div>
      <b>Example Images</b>
    </div>
    <script type="text/javascript" src="<?php echo($this->config->item('js_path'));?>jquery.cycle.all.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.slideshow').cycle({
        fx: 'fade' // choose your transition type, ex: fade, scrollUp, shuffle, etc...
      });
    });
    </script>

  </div><br/><?php $_SESSION['redirect_after_crop'] = current_url();?>
  <input type="submit" name="upload" id="green_button" value="Upload Image"/>
</form>
</section>

<a class="button" name="save_and_continue_button" id="save_and_continue_button">Save and Continue</a><br/>
<a class="button" href="<?php echo base_url();?>form/participation-agreement">Previous Page</a>


</form>
</article>
<script>
var ACCOUNT_ID = <?php echo $_SESSION['ACCOUNT_id']?>;
var TOKEN = '<?php echo $_SESSION['token'];?>';
var WORKCREW_ELIGIBLE = <?php echo $workcrew_eligible;?>;

function strip_double_quotes(vars){
  if(typeof vars === 'string' || vars instanceof String){
    return vars.replace(/"/g, '&quot;');
  } else {
    return vars;
  }
}

function update_percent_complete(){
  var url = '<?php echo $this->config->item('api_url');?>v2/application/<?php echo $_SESSION['APPLICATION_id'];?>/percent-complete';
  var data = jQuery.parseJSON('{"api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    dataType: 'json',
    success: function(response){
      percent = Math.round(response.data.preselection.total*100);
      animate();
    }
  });
};


function autosave(name, val, url){
  val = strip_double_quotes(val);
  $('#savebar').addClass('saving');
  $('#savebar').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
    $('#savebar').removeClass('saving');
  });
  var data = jQuery.parseJSON('{"'+name+'": "'+val+'", "api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    dataType: 'json',
    success: function(data){
      // Sucess
      $('[name="'+name+'"]').removeClass('has_error');
      $('#'+name+'_error').remove();
      update_percent_complete();
    },
    error: function(data){
      // Failure to save data
      response = data.responseJSON;
      data = response.data;
      $('#savebar').removeClass('saving').addClass('saved');
      $('[name="'+name+'"]').addClass('has-error');
      error_id = name+'_error';
      if($("#"+error_id).length == 0){
        $('<span id="'+error_id+'" class="error_message">'+data.error_message+'</span>').insertAfter('[name="'+name+'"]');
      }
    }
  });
};


$('.autosave').change(function(){
  name = $(this).attr('name');
  val = $(this).val();
  autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
  autosave(name, val, autosave_url);
});


$(".checkbox").click(function(){
  var is_clicked = $(this).is(':checked');
  var name = $(this).attr('name');
  if(is_clicked){
    var val = 1;
  } else {
    var val = 0;
  }
  autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
  autosave(name, val, autosave_url);
});

$('#save_and_continue_button').click(function(){
  $('#form').submit();
});

$('#work_crew_instructions_button').click(function(){
    autosave_url = '<?php echo $this->config->item('api_url');?>v2/application/<?php echo $_SESSION['APPLICATION_id'];?>';
    autosave('read_workcrew_instructions', 1, autosave_url);
});
</script>
