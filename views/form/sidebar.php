<?php $svg_path = $this->config->item('svg_path');?>
<aside class="three columns" id="sidebar">
  <ul>
    <li><b><a href="<?php echo base_url();?>dashboard/">Back to the Dashboard</a></b></li>
    <script type="text/javascript" src="https://d3js.org/d3.v3.min.js"></script>
    <li><b><a name="#"><?php echo $_SESSION['application_full_name'];?></a></b></li>
    <li>
      <ul>
          <li id="terms_and_conditions">
            <div><a class="<?php echo sidebar_indicator($percent_complete, 'terms_and_conditions');?>" href="<?php echo base_url();?>form/participation-agreement">Participation Agreement</a></div>
          </li>
        <li id="preliminary_questions">
          <div><a class="<?php echo sidebar_indicator($percent_complete, 'preliminary_questions');?>" href="<?php echo base_url();?>form/preliminary-questions">Preliminary Questions</a></div>
        </li>
        <?php if($application_details->apply_for_workcrew):?>
        <li id="medical">
          <div><a class="<?php echo sidebar_indicator($percent_complete, 'medical');?>" href="<?php echo base_url();?>form/medical">Medical</a></div>
        </li>
        <?php endif;?>
        <li id="contact_information">
          <div><a class="<?php echo sidebar_indicator($percent_complete, 'contact_information');?>" href="<?php echo base_url();?>form/contact-information">Contact Information</a></div>
        </li>
        <li id="ecclesiastical_leader">
          <div><a class="<?php echo sidebar_indicator($percent_complete, 'ecclesiastical_leader');?>" href="<?php echo base_url();?>form/ecclesiastical-leader">Ecclesiastical Leader</a></div>
        </li>
        <li id="experience">
          <div><a class="<?php echo sidebar_indicator($percent_complete, 'experience');?>" href="<?php echo base_url();?>form/experience">Experience</a></div>
        </li>
        <li id="miscellaneous">
          <div><a class="<?php echo sidebar_indicator($percent_complete, 'miscellaneous');?>" href="<?php echo base_url();?>form/misc">Miscellaneous</a></div>
        </li>
        <li id="review">
          <div><a class="<?php echo ($application_details->status != 'Started')?'completed':'not_completed';?>" href="<?php echo base_url();?>form/review">Review</a></div>
        </li>
      </ul>
    </li>
  </ul>
  <div class="widget">
    <div class="header">Percent Complete</div>
    <div id="chart" class="chart_container"></div>
  </div>
  <script type="text/javascript">
  var percent = <?php echo round(($percent_complete->preselection->total*100),0);?>;
  var ratio=percent/100;
  var pie=d3.layout.pie()
  .value(function(d){return d})
  .sort(null);
  var w = h = 200;
  var outerRadius=(w/2)-10;
  var innerRadius=90;

  var arc=d3.svg.arc()
  .innerRadius(0)
  .outerRadius(outerRadius)
  .startAngle(0)
  .endAngle(2*Math.PI);

  var arcLine=d3.svg.arc()
  .innerRadius(innerRadius)
  .outerRadius(outerRadius-10)
  .startAngle(0);

  var svg=d3.select("#chart")
  .append("svg")
  .attr({
    width:w,
    height:h,
    class:'shadow'
  }).append('g')
  .attr({
    transform:'translate('+w/2+','+h/2+')'
  });

  var defs = svg.append("svg:defs");

  var inset_shadow = defs.append("svg:filter")
  .attr("id", "inset-shadow");

  inset_shadow.append("svg:feOffset")
  .attr({
    dx:0,
    dy:0
  });

  inset_shadow.append("svg:feGaussianBlur")
  .attr({
    stdDeviation:4,
    result:'offset-blur'
  });

  inset_shadow.append("svg:feComposite")
  .attr({
    operator:'out',
    in:'SourceGraphic',
    in2:'offset-blur',
    result:'inverse'
  });

  inset_shadow.append("svg:feFlood")
  .attr({
    'flood-color':'black',
    'flood-opacity':.7,
    result:'color'
  });

  inset_shadow.append("svg:feComposite")
  .attr({
    operator:'in',
    in:'color',
    in2:'inverse',
    result:'shadow'
  });

  inset_shadow.append("svg:feComposite")
  .attr({
    operator:'over',
    in:'shadow',
    in2:'SourceGraphic'
  });

  var path=svg.append('path')
  .attr({
    d:arc
  })
  .style({
    fill:'#efefef',
    filter:'url(#inset-shadow)'
  });


  var pathForeground=svg.append('path')
  .datum({endAngle:0})
  .attr({
    d:arcLine
  })
  .style({
    fill:'#559259',
    filter:'url(#inset-shadow)'
  });


  var middleCount=svg.append('text')
  .datum(0)
  .text(function(d){
    return d;
  })
  .attr({
    class:'middleText',
    'text-anchor':'middle',
    dy:25
  })
  .style({
    fill:d3.rgb('#888888'),
    'font-size':'55px',
    filter:'url(#inset-shadow)'
  });

  var current_percent=0;

  var arcTween=function(transition, newValue, current_percent) {
    transition.attrTween("d", function (d) {
      var interpolate = d3.interpolate(d.endAngle, ((2*Math.PI))*(newValue/100));
      var interpolateCount = d3.interpolate(current_percent, newValue);
      return function (t) {
        d.endAngle = interpolate(t);
        middleCount.text(Math.floor(interpolateCount(t))+'%');
        return arcLine(d);
      };
    });
  };

  var animate=function(){
    if(current_percent != percent){
      pathForeground.transition()
      .duration(750)
      .ease('cubic')
      .call(arcTween, percent, current_percent);
      current_percent = percent;
    }
  };

  setTimeout(animate,0);
  </script>
</aside>
