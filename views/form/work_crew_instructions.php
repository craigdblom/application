<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/png" href="<?php echo $this->config->item('img_path');?>favicon.png"/>
  <!-- FONT -->
  <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
  <style>
  body{
    font-size: 15px;
    color: #808080;
    font-family: "Open Sans", 'Helvetica Neue', Helvetica, Arial, sans-serif;
    line-height: 1.5em;
  }
  1{
    font-size: 1.714285714em; /* 24 / 14 */
    line-height: 1.5em; /* 21 / 14 */
  }

  a h1 {
    color: #fff;
  }

  h2{
    font-size: 1.5em; /* 21 / 14 */
    line-height: 1.5em; /* 21 / 14 */
  }

  h3{
    font-size: 1.25em;
    line-height: 1.5em;
  }

  p{
    font-size: 1em; /* 14 / 14 */
    line-height: 1.5em; /* 21 / 14 */
    margin-bottom: 1em; /* 21 / 14 */
  }

  .red {
    color: #EF4C3C;
  }

  </style>
  <title>Hill Cumorah Pageant : Work Crew Instructions</title>
</head>

<body class="cf">
  <div class="card">
    <h1>Work Crew Application</h1>
    <p>Thank you for your interest in being in Work Crew. Please keep in mind this experience is not for everyone. At times it can be physically demanding.</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <h2>The Hill Cumorah Pageant Work Crew Application Information</h2>
    <p>If you are NOT a young man 17 to 19 years of age by July 1, <?php echo $_SESSION['current_application_year'];?> apply to be part of the cast.</p>
    <p>If you meet the age requirements and want to be a part of the crew that erects, maintains, and disassembles the stage and provides all of the special staging effects during the pageant performances, continue reading this information sheet and follow all directions at the end of this document.</p>
    <p>Welcome to the application page for the Work Crew of the Hill Cumorah Pageant. To apply a young man must be 17 to 19 years of age by July 1, <?php echo $_SESSION['current_application_year'];?> and be in excellent health.</p>
    <p>Twenty-eight young men will be selected to make up the <b>“Pageant Work Crew”.</b></p>
    <p>The 28 young men accepted are required to have a physical examination, Tetanus immunization within the past 10 years and be willing to conduct themselves according to the following requirements:
      <ul>
        <li>Be a worthy member of the Church at all times.</li>
        <li>Read from the Book of Mormon daily.</li>
        <li>Have a standard missionary haircut before reporting to the hill and be clean shaven each day.</li>
        <li>Follow missionary standards at all times.</li>
        <li>Communicate with friends at home by letter only. (A phone call to the family upon arrival and just before departure is acceptable.)</li>
        <li>Work long hours each day (12 to 16 hours).</li>
        <li>Be obedient and willing to follow instructions.</li>
        <li>Be able to peacefully reside with <span class="red">27</span> other young men in a bunkhouse.</li>
        <li>Be patient with others.</li>
        <li>Be able to lead others in work assignments.</li>
        <li>COMPLETE 11 PREPARATION ASSIGNMENTS, AS ASSIGNED BY THE WORK CREW DIRECTOR, ON-TIME AND BEFORE ARRIVING AT THE HILL.</li>
      </ul>
    </p>
    <h2>CRITICAL DATES</h2>
    <p>Work Crew Arrival: Friday, June 28, <?php echo $_SESSION['current_application_year'];?><br/>
      Work Crew Released: Friday, July 26, <?php echo $_SESSION['current_application_year'];?>
    </p>
    <h2>FEES/TRANSPORTATION/HOUSING/MEALS</h2>
    <p>Your cost to be part of the work crew if selected will be $135. The balance of your participation cost for transportation, food, etc. will be covered by the Pageant.</p>
    <h2>TO APPLY</h2>
    <p>Fill out the online Work Crew application form completely.  You must check the checkbox indicating you are applying for work crew.</p>
    <h2>ADDITIONAL APPLICATION DOCUMENTATION</h2>
    <p>
      <ol>
        <li>As part of the online application, you will be asked to write a "personal letter of recommendation" for yourself.  Please include the following in your letter:
          <ul>
            <li>Why you should be considered for the work crew.</li>
            <li>Any skills and/or experiences that you feel would be particularly helpful to your participation on the work crew.</li>
            <li>Employment, service and leadership experiences you have had.</li>
            <li>Testimony Building Experiences you have had.</li>
          </ul>
        </li>
        <li>Upon submission of your application your Bishop/Branch President will be asked for a "Letter of Recommendation." Make sure your Bishop/Branch President knows the date you need his letter <b>(no later than November 1, <?php echo $_SESSION['current_application_year']-1;?>)</b>.</li>
        <li>Please complete the online medical section in its entirety.  IF YOU ARE SELECTED as a member of the work crew, you will also be required to submit a physician-signed physical (form will be provided to you) before you report to the Hill.</li>
      </ol>
    </p>
    <p>You must complete and submit all sections of the application by November 1, <?php echo $_SESSION['current_application_year']-1;?>.  Applications submitted after this date will not be accepted.</p>
    <h2>QUESTIONS</h2>
    <p>Please contact Brother David Zundel
      <ol>
        <li>By email: castadmin@hcpageant.com</li>
        <li>By Letter:<br/>
          David Zundel<br/>
          The Hill Cumorah Pageant<br/>
          659 State Route 21<br/>
          Palmyra, New York 14522</li>
          <li>By phone: 585-594-4706</li>
        </ol>
      </p>
    </div>
  </body>
  </html>
