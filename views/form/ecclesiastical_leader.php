<?php echo $sidebar;?>

<article class="card nine columns" id="application_form">
  <h2>Ecclesiastical Leader</h2>
  <?php if(isset($error_message)):?>
    <div id="error_message"><?php echo $error_message;?></div>
  <?php endif;?>
  <h3>Who is your Ecclesiastical Leader?</h3>
  <p>All pageant participants need an endorsement from their ecclesiastical leader.  Please provide contact information for the applicant's ecclesiastical leader below, as all pageant participants are required to have an ecclesiastical endorsement. The HCP will email the ecclesiastical leader with instructions about how to complete the endorsement.</p>
  <?php if($ecclesiastical_leader_details):# The ecclesiastical leader id is set so show the table that gives the status?>
    <form action="<?php echo current_url();?>" method="post">
      <input id="action" type="hidden" name="action" value="save_and_continue"/>
      <section class="scroll_x">
        <table style="width:100%;">
          <tr>
            <td>Leader</td>
            <td><?php echo ($ecclesiastical_leader_details->calling == 'Bishop')?'Ward':'Branch';?> Name</td>
            <td>Stake Name</td>
            <td class="center">Action</td>
          </tr>
          <tr>
            <td><?php echo $ecclesiastical_leader_details->calling.' '.$ecclesiastical_leader_details->last_name;?></td>
            <td><?php echo $ecclesiastical_leader_details->unit_name;?></td>
            <td><?php echo $ecclesiastical_leader_details->stake_name;?></td>
            <td><a id="remove" class="remove button">Remove</a></td>
          </tr>
        </table>
      </section>
    <?php else: # The ecclesiastical leader id is not set so show the search/create tools?>
      <section id="search_ecclesiastical_leader">
        <form id="form" action="<?php echo current_url();?>" method="post">
          <input name="search" id="search" type="text" placeholder="Search by leader or church unit name" />
          <div id="search_results" class="hidden"></div>
        </form>
      </section>
      <form id="add_form" action="<?php echo current_url();?>" method="post" class="hidden">
        <input id="ECCLESIASTICAL_LEADER_id" name="ECCLESIASTICAL_LEADER_id" type="hidden"/>
        <input name="action" value="add" type="hidden" />
      </form>
      <?php if($ecclesiastical_leader_suggestions):?>
        <h3>Is this your ecclesiastical leader?</h3>
        <p>Click on the leader's name below if they should be listed as your ecclesiastical leader<br/>
      <?php foreach ($ecclesiastical_leader_suggestions as $key => $suggestion):?>
        <a href="#" data-id="<?php echo $suggestion->id;?>" class="add_suggestion"><?php echo $suggestion->calling.' '.$suggestion->last_name;?></a><br/>
      <?php endforeach;?>
    </p>
    <?php endif;?>
      <form id="form" action="<?php echo current_url();?>" method="post">
        <section id="add_ecclesiastical_leader" class="hidden">
          <input name="id" id="id" type="hidden" />
          <label>Calling</label>
          <?php echo form_dropdown('calling', array(''=>'Select One','Bishop'=>'Bishop', 'Branch President'=>'Branch President'), '', 'class="ecclesiastical_leader_autosave"');?>
          <label>First Name</label>
          <input class="ecclesiastical_leader_autosave" type="text" name="first_name" id="first_name" />
          <label>Last Name</label>
          <input class="ecclesiastical_leader_autosave" type="text" name="last_name" id="last_name" />
          <label>Email Address</label>
          <input class="ecclesiastical_leader_autosave" type="text" name="email_address" id="email_address" />
          <label>Ward/Branch Name</label>
          <input class="ecclesiastical_leader_autosave" type="text" name="unit_name" id="unit_name" />
          <label>Stake Name</label>
          <input class="ecclesiastical_leader_autosave" type="text" name="stake_name" id="stake_name" />
        </section>
      <?php endif;?>
      <input type="submit" name="save_and_continue" value="Save and Continue" /><br/>
      <a class="button" href="<?php echo base_url();?>form/contact-information">Previous Page</a>

    </form>
  </article>
  <script>
  $(document).ready(function(){
    var API_TOKEN = '<?php echo $this->session->token;?>';
    var ACCOUNT_ID = <?php echo $this->session->ACCOUNT_id;?>;
    <?php if(isset($application_details->ECCLESIASTICAL_LEADER_ID) &&  $application_details->ECCLESIASTICAL_LEADER_ID != null):?>
    var ECCLESIASTICAL_LEADER_ID = <?php echo $application_details->ECCLESIASTICAL_LEADER_id;?>;
    <?php endif;?>

    function update_percent_complete(){
      var url = '<?php echo $this->config->item('api_url');?>v2/application/<?php echo $_SESSION['APPLICATION_id'];?>/percent-complete';
      var data = jQuery.parseJSON('{"api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
      $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: 'json',
        success: function(response){
          percent = Math.round(response.data.preselection.total*100);
          animate();
        }
      });
    };

    $('body').on('click', '.ecclesiastical_leader_search_result', function(){
      var val = $(this).attr('name');
      $('#ECCLESIASTICAL_LEADER_id').val(val);
      $('#add_form').submit();
    });

    $('a.add_suggestion').click(function(){
      var id = $(this).attr('data-id');
      $('#ECCLESIASTICAL_LEADER_id').val(id);
      $('#add_form').submit();
    });


    $('body').on('click','#add_ecclesiastical_leader_link', function(){
      var name_guess = $('#search').val();
      $.ajax({
        type: 'POST',
        url: '<?php echo $this->config->item('api_url');?>v2/ecclesiastical-leader/',
        data: { 'api_credentials':{'ACCOUNT_id': ACCOUNT_ID, 'token': API_TOKEN}, 'first_name':name_guess, 'create_hash':1},
        dataType: 'json'
      }).done(function(response) {
        var id = response.data.id;
        $('#id').val(id);
        $('#first_name').val(response.data.first_name);
        $('#add_ecclesiastical_leader').removeClass('hidden');
        $('#search_ecclesiastical_leader').addClass('hidden');
        // Link the ecclesiastical leader to the application
        autosave_url = '<?php echo $this->config->item('api_url');?>v2/application/<?php echo ($_SESSION['APPLICATION_id']);?>';
        autosave('ECCLESIASTICAL_LEADER_id', id, autosave_url);
      });
    });


    $('#search').keyup(function(){
      search_terms = $(this).val();
      if(search_terms.length > 4){
        $.ajax({
          type: 'POST',
          url: '<?php echo $this->config->item('api_url');?>v2/ecclesiastical-leader/search-results',
          data: { 'api_credentials':{'ACCOUNT_id': ACCOUNT_ID, 'token': API_TOKEN}, 'search_terms': search_terms},
          dataType: 'json'
        }).done(function(response) {
          $('#search_results').empty();
          var displayed_fuzzy_header = false;
          $.each(response.data.search_results, function(index, search_result) {
            if(search_result.similarity_score < 1 && !displayed_fuzzy_header){
              $('#search_results').append('<h4>You might have meant</h4>');
              displayed_fuzzy_header = true;
            }
            $('#search_results').append('<a class="ecclesiastical_leader_search_result" name="'+search_result.id+'">'+search_result.name+'</a>').removeClass('hidden');
          });
          $('#search_results').append('<b><a id="add_ecclesiastical_leader_link" name="add_ecclesiastical_leader_link">Add a New Ecclesiastical Leader Record</a></b>');
        });
      } else {
        if(search_terms.length>0){
          $('#search_results').html('Keep typing for suggestions.');
        } else {
          $('#search_results').empty().addClass('hidden');
        }
      }
    });


    function strip_double_quotes(vars){
      if(typeof vars === 'string' || vars instanceof String){
        return vars.replace(/"/g, '&quot;');
      } else {
        return vars;
      }
    }


    function autosave(name, val, url){
      val = strip_double_quotes(val);
      $('#savebar').addClass('saving');
      $('#savebar').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
        $('#savebar').removeClass('saving');
      });
      var data = jQuery.parseJSON('{"'+name+'": "'+val+'", "api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+API_TOKEN+'"}}');
      $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: 'json',
        success: function(data){
          // Sucess
          $('[name="'+name+'"]').removeClass('has_error');
          $('#'+name+'_error').remove();
          update_percent_complete();
        },
        error: function(data){
          // Failure to save data
          response = data.responseJSON;
          data = response.data;
          $('#savebar').removeClass('saving').addClass('saved');
          $('[name="'+name+'"]').addClass('has_error');
          error_id = name+'_error';
          if($("#"+error_id).length == 0){
            $('<span id="'+error_id+'" class="error_message">'+data.error_message+'</span>').insertAfter('[name="'+name+'"]');
          }
        }
      });
    };


    $('.autosave').change(function(){
      name = $(this).attr('name');
      val = $(this).val();
      autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
      autosave(name, val, autosave_url);
    });


    $('.ecclesiastical_leader_autosave').change(function(){
      name = $(this).attr('name');
      val = $(this).val();
      id = $('#id').val();
      autosave_url = '<?php echo $this->config->item('api_url');?>v2/ecclesiastical-leader/'+id;
      autosave(name, val, autosave_url);
    });


    $('#remove').click(function(){
      $('#action').val('remove');
      $('form').submit();
    });

  });
  </script>
