<?php echo $sidebar;?>

<article class="card nine columns" id="application_form">
  <h2>Contact Information</h2>
  <?php if(isset($error_message)):?>
    <div id="error_message"><?php echo $error_message;?></div>
  <?php endif;?>
  <?php if($workcrew_eligible && $application_details->apply_for_cast):?>
      <div id="info_message">Did you know you are eligible to be on the HCP Work Crew?  If you would like to be a part of the HCP Work Crew go back to the <a href="<?php echo base_url();?>form/preliminary-questions">Preliminary Questions</a> page, read the Work Crew instructions and check the checkbox next to Work Crew.</div>
  <?php endif;?>
  <form action="<?php echo current_url();?>" method="post">
    <section>
      <h3>Where do you live?</h3>
      <a name="address"></a>
      <div class="cf">
        <div class="span12">
          <label>Street address</label>
          <input class="autosave" type="text" name="address_line_1" value="<?php echo $application_details->address_line_1;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
      </div>

      <div class="cf">
        <div class="span12">
          <label>Street address (continued)</label>
          <input class="autosave" type="text" name="address_line_2" value="<?php echo $application_details->address_line_2;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
      </div>

      <div class="cf row">
        <div class="seven columns">
          <label>City</label>
          <input class="autosave" type="text" name="city" value="<?php echo $application_details->city;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
        <div class="three columns">
          <label>State/Providence/Region</label>
          <input class="autosave" type="text" name="state" value="<?php echo $application_details->state;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
        <div class="two columns">
          <label>Zip/Postal code</label>
          <input class="autosave" type="text" name="zip" value="<?php echo $application_details->zip;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
      </div>

      <div class="cf">
        <div class="span12">
          <label>Country</label>
          <input class="autosave" type="text" name="country" value="<?php echo $application_details->country;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
      </div>
      <!--<input type="button" id="validate_address" value="Validate Address"/>-->
      <div id="map"></div>
    </section>

    <section>
      <h3>What is your email address?</h3><a name="email-address"></a>
      <label>Email address</label>
      <input class="autosave" type="email" name="email_address" value="<?php echo $application_details->email_address;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
    </section>

    <section>
      <h3>What is your phone number?</h3><a name="cell-phone"></a>
      <div class="cf">
        <div class="span6">
          <label>Cell phone</label>
          <input class="autosave" type="tel" name="cell_phone" value="<?php echo $application_details->cell_phone;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
        </div>
        <div class="span6 col">
          <label>Cell carrier</label>
          <input class="autosave" type="text" name="cell_phone_carrier" value="<?php echo $application_details->cell_phone_carrier;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
          <!--input list="cell_phone_carrier_list" class="autosave" type="text" name="cell_phone_carrier" value="<?php echo $application_details->cell_phone_carrier;?>" data-endpoint="individual/<?php echo $_SESSION['APPLICATION_INDIVIDUAL_id'];?>" />
          <datalist id="cell_phone_carrier_list">
            <option value="ATT Wireless"/>
          <option value="Alaska Communications Systems"/>
          <option value="Alltel"/>
          <option value="Alltel Wireless"/>
          <option value="Bellsouth"/>
          <option value="Boost"/>
          <option value="CellularOne"/>
          <option value="Centennial Wireless"/>
          <option value="Cincinnati Bell"/>
          <option value="Cingular"/>
          <option value="Cingular (GoPhone prepaid)"/>
          <option value="Edge Wireless"/>
          <option value="Golden State Cellular"/>
          <option value="Metro PCS"/>
          <option value="My Cingular"/>
          <option value="O2"/>
          <option value="Orange"/>
          <option value="Pioneer Cellular"/>
          <option value="Qwest"/>
          <option value="Rogers Wireless"/>
          <option value="Sprint (Nextel)"/>
          <option value="Sprint PCS"/>
          <option value="Straight Talk"/>
          <option value="Syringa WirelessUSA"/>
          <option value="T-Mobile"/>
          <option value="Teleflip"/>
          <option value="Telus Mobility"/>
          <option value="US Cellular"/>
          <option value="UnicelUSA"/>
          <option value="Verizon"/>
          <option value="ViaeroUSA"/>
          <option value="Virgin Mobile"/>
          <option value="Claro Puerto Rico"/>
        </datalist>-->
      </div>
    </div>
    </section>

    <section class="span12 col"><a name="show-in-directory"></a>
      <h3>Do you want to be listed in the HCP directory?</h3>
      <?php echo form_dropdown('show_in_directory', array('0'=>'No','1'=>'Yes'), $application_details->show_in_directory, 'class="autosave" data-endpoint="individual/'.$_SESSION['APPLICATION_INDIVIDUAL_id'].'"');?>
    </section>

    <input type="submit" name="save_and_continue" value="Save and Continue"/><br/>
    <a class="button" href="<?php echo ($application_details->apply_for_workcrew)? base_url().'form/medical': base_url().'form/preliminary-questions';?>">Previous Page</a>

  </form>
</article>
<script>
var ACCOUNT_ID = <?php echo $_SESSION['ACCOUNT_id']?>;
var TOKEN = '<?php echo $_SESSION['token'];?>';


function strip_double_quotes(vars){
  if(typeof vars === 'string' || vars instanceof String){
    return vars.replace(/"/g, '&quot;');
  } else {
    return vars;
  }
}

function update_percent_complete(){
  var url = '<?php echo $this->config->item('api_url');?>v2/application/<?php echo $_SESSION['APPLICATION_id'];?>/percent-complete';
  var data = jQuery.parseJSON('{"api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    dataType: 'json',
    success: function(response){
      percent = Math.round(response.data.preselection.total*100);
      animate();
    }
  });
};

function autosave(name, val, url){
  val = strip_double_quotes(val);
  $('#savebar').addClass('saving');
  $('#savebar').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
    $('#savebar').removeClass('saving');
  });
  var data = jQuery.parseJSON('{"'+name+'": "'+val+'", "api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    dataType: 'json',
    success: function(data){
      // Sucess
      $('[name="'+name+'"]').removeClass('has_error');
      $('#'+name+'_error').remove();
      update_percent_complete();
    },
    error: function(data){
      // Failure to save data
      response = data.responseJSON;
      data = response.data;
      $('#savebar').removeClass('saving').addClass('saved');
      $('[name="'+name+'"]').addClass('has_error');
      error_id = name+'_error';
      if($("#"+error_id).length == 0){
        $('<span id="'+error_id+'" class="error_message">'+data.error_message+'</span>').insertAfter('[name="'+name+'"]');
      }
    }
  });
};


$('.autosave').change(function(){
  name = $(this).attr('name');
  val = $(this).val();
  autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
  autosave(name, val, autosave_url);
});
</script>
