<?php function get_medical_conditions($application_details){
  $medical_conditions = '';
  if($application_details->has_add_or_adhd){$medical_conditions.='ADD/ADHD, ';}
  if($application_details->has_aspergers_syndrome){$medical_conditions.="Asperger's Syndrome, ";}
  if($application_details->has_asthma){$medical_conditions.='Asthma, ';}
  if($application_details->has_autism){$medical_conditions.='Autism, ';}
  if($application_details->has_bleeding_disorder){$medical_conditions.='Bleeding Disorder, ';}
  if($application_details->has_blood_borne_disease){$medical_conditions.='Blood Borne Disease, ';}
  if($application_details->has_clotting_disorder){$medical_conditions.='Clotting Disorder, ';}
  if($application_details->has_diabetes){$medical_conditions.='Diabetes, ';}
  if($application_details->has_emotional_personality_disorder){$medical_conditions.='Emotional/Personality Disorder, ';}
  if($application_details->has_epilepsy_seizure_disorder){$medical_conditions.='Epilepsy/Seizure Disorder, ';}
  if($application_details->has_heart_problems_arrhythmia){$medical_conditions.='Heart Problems/Arrhythmia, ';}
  if($application_details->has_heart_illness){$medical_conditions.='Heart Illness, ';}
  if($application_details->has_high_blood_pressure){$medical_conditions.='High Blood Pressure, ';}
  if($application_details->has_gi_disorder_crohns_uc){$medical_conditions.='GI Disorder Crohns/UC, ';}
  if($application_details->has_kidney_problems){$medical_conditions.='Kidney Problems, ';}
  if($application_details->has_lung_problems){$medical_conditions.='Lung Problems, ';}
  if($application_details->has_marfan_syndrome){$medical_conditions.='Marfan Syndrome, ';}
  if($application_details->has_musculoskeletal){$medical_conditions.='Musculoskeletal, ';}
  if($application_details->has_sickle_cell_disease){$medical_conditions.='Sickle Cell Disease, ';}
  if($application_details->has_sleeping_disorder){$medical_conditions.='Sleeping Disorder, ';}
  if($application_details->has_stroke){$medical_conditions.='Stroke, ';}


  if(strlen($medical_conditions) < 1){
    return(FALSE);
  } else {
    $medical_conditions = substr($medical_conditions, 0, -3);
    return($medical_conditions);
  }
}?>
<?php echo $sidebar;?>

<article class="card nine columns" id="application_form">
  <h2>Review</h2>
  <?php if(isset($error_message)):?>
    <div id="error_message"><?php echo $error_message;?></div>
  <?php endif;?>
  <p><b>Instructions:</b> This application is <?php echo round(($percent_complete->preselection->total*100),0);?>% complete.
    <?php if($percent_complete->preselection->total < 1):?>The incomplete questions are highlighted below.  Once you have answered all questions you may mark it as reviewed.
    <?php else:?>Please review your responses.  If everything is in order, mark it as reviewed.  Once all the applications are reviewed you can submit your application(s) to the Pageant Presidency on the <a href="<?php echo base_url();?>dashboard">dashboard</a>.
    <?php endif;?>
  </p>

  <section class="cf">
    <hr/>
    <h3>Preliminary Questions</h3>
    <?php $image = ($application_details->profile_image_uploaded=="1")?$application_details->INDIVIDUAL_id:'no_image'; ?>
    <span<?php if($image == 'no_image'):?> class="review_highlight"<?php endif;?>><div id="review_image" class="rounded_image"><img src="<?php echo $this->config->item('img_path') . $image.'.jpg?'.md5(time());?>" /></div> <a href="<?php echo base_url();?>form/preliminary-questions#profile-image" class="show_when_highlighted"><br/>fix this</a></span>
    <p>
      <span<?php if(($percent_complete->requirements_met->preliminary_questions->first_name + $percent_complete->requirements_met->preliminary_questions->last_name) < 2):?> class="review_highlight"<?php endif;?>>Name: <?php echo $application_details->first_name.' '.$application_details->last_name;?> <a href="<?php echo base_url();?>form/preliminary-questions#full-name" class="show_when_highlighted">fix this</a></span><br/>
      <span<?php if($percent_complete->requirements_met->preliminary_questions->date_of_birth == 0):?> class="review_highlight"<?php endif;?>>Birth Date: <?php echo $application_details->date_of_birth;?> <a href="<?php echo base_url();?>form/preliminary-questions#date-of-birth" class="show_when_highlighted">fix this</a></span><br/>
      <span<?php if($percent_complete->requirements_met->preliminary_questions->gender == 0):?> class="review_highlight"<?php endif;?>>Gender: <?php if($application_details->gender == 'M'){echo 'Male';} elseif($application_details->gender == 'F'){echo 'Female';}?> <a href="<?php echo base_url();?>form/preliminary-questions#gender" class="show_when_highlighted">fix this</a></span><br/>
      <span<?php if($percent_complete->requirements_met->preliminary_questions->single == 0):?> class="review_highlight"<?php endif;?>>Marital status: <?php echo ($application_details->single == 1)?'Single':'Married';?> <a href="<?php echo base_url();?>form/preliminary-questions#marital-status" class="show_when_highlighted">fix this</a></span><br/>
      <span<?php if($applying_for == 'N/A'):?> class="review_highlight"<?php endif;?>>Applying for: <?php echo ($applying_for == 'N/A')?'None':$applying_for;?> <a href="<?php echo base_url();?>form/preliminary-questions#apply-for" class="show_when_highlighted">fix this</a></span><br/>
    <?php if($application_details->apply_for_workcrew):?>
      <span<?php if($percent_complete->requirements_met->preliminary_questions->read_workcrew_instructions == 0):?> class="review_highlight"<?php endif;?>>Read Workcrew Instructions: <?php echo ($application_details->read_workcrew_instructions == 1)?'Yes':'No';?> <a href="<?php echo base_url();?>form/preliminary-questions#workcrew-instructions" class="show_when_highlighted">fix this</a></span>
    <?php endif;?>
    </p>
  </section>
<?php if($application_details->apply_for_workcrew):?>
  <section class="cf">
    <h3>Medical</h3>
    <?php if($applying_for == 'N/A'):?><span class="review_highlight">You must specify how you want to participate in the pageant first</span><?php endif;?>
    <?php if($application_details->apply_for_workcrew == 1):?>
    <div class="span6">
      <p>
        <span<?php if($percent_complete->requirements_met->medical->height_feet == 0 || $percent_complete->requirements_met->medical->height_inches == 0):?> class="review_highlight"<?php endif;?>>Height: <?php echo $application_details->height_feet."' ".$application_details->height_inches.'"';?> <a href="<?php echo base_url();?>form/medical#height" class="show_when_highlighted">fix this</a></span><br/>
        <span<?php if($percent_complete->requirements_met->medical->weight_lbs == 0):?> class="review_highlight"<?php endif;?>>Weigh (lbs): <?php echo $application_details->weight_lbs;?> <a href="<?php echo base_url();?>form/medical#weight" class="show_when_highlighted">fix this</a></span><br/>
        <span<?php if($percent_complete->requirements_met->medical->tetanus_immunization_year == 0):?> class="review_highlight"<?php endif;?>>Year of Last Tetanus Shot: <?php echo $application_details->tetanus_immunization_year;?> <a href="<?php echo base_url();?>form/medical#tetanus" class="show_when_highlighted">fix this</a></span>
        </p>
    </div>
    <div class="span6 col">
      <p>
        <span<?php if($percent_complete->requirements_met->medical->drug_allergy == 0):?> class="review_highlight"<?php endif;?>>Drug Allergy: <?php echo yes_no($application_details->drug_allergy);?> <a href="<?php echo base_url();?>form/medical#allergies" class="show_when_highlighted">fix this</a></span><br/>
        <span<?php if($percent_complete->requirements_met->medical->food_allergy == 0):?> class="review_highlight"<?php endif;?>>Food Allergy: <?php echo yes_no($application_details->food_allergy);?> <a href="<?php echo base_url();?>form/medical#allergies" class="show_when_highlighted">fix this</a></span><br/>
        <span<?php if($percent_complete->requirements_met->medical->plant_allergy == 0):?> class="review_highlight"<?php endif;?>>Plant Allergy: <?php echo yes_no($application_details->plant_allergy);?> <a href="<?php echo base_url();?>form/medical#allergies" class="show_when_highlighted">fix this</a></span><br/>
        <span<?php if($percent_complete->requirements_met->medical->insect_sting_allergy == 0):?> class="review_highlight"<?php endif;?>>Insect Sting Allergy: <?php echo yes_no($application_details->insect_sting_allergy);?> <a href="<?php echo base_url();?>form/medical#allergies" class="show_when_highlighted">fix this</a></span>
      </p>
  </div>

  <div class="span6">
    <p>
      <span<?php if($percent_complete->requirements_met->medical->medications == 0):?> class="review_highlight"<?php endif;?>>Do you regularly take any prescription medications: <?php echo yes_no($application_details->medications);?> <a href="<?php echo base_url();?>form/medical#medications" class="show_when_highlighted">fix this</a></span><br/>
      <?php if($application_details->medications == 1):?><span<?php if($percent_complete->requirements_met->medical->medications_list == 0):?> class="review_highlight"<?php endif;?>>Explain: <?php echo $application_details->medications_list;?> <a href="<?php echo base_url();?>form/medical#medications-followup" class="show_when_highlighted">fix this</a></span><br/><?php endif;?>
      <span<?php if($percent_complete->requirements_met->medical->difficulty_running_walking_seeing_hearing == 0):?> class="review_highlight"<?php endif;?>>Difficulty walking, running, seeing, or hearing: <?php echo yes_no($application_details->difficulty_running_walking_seeing_hearing);?> <a href="<?php echo base_url();?>form/medical#walk-see-run" class="show_when_highlighted">fix this</a></span><br/>
      <?php if($application_details->difficulty_running_walking_seeing_hearing == 1):?><span<?php if($percent_complete->requirements_met->medical->difficulty_note == 0):?> class="review_highlight"<?php endif;?>>Explain: <?php echo $application_details->difficulty_note;?> <a href="<?php echo base_url();?>form/medical#walk-see-run-followup" class="show_when_highlighted">fix this</a></span><br/><?php endif;?>
    </p>
  </div>
  <div class="span6 col">
    <p>
      <span<?php if($percent_complete->requirements_met->medical->special_diet == 0):?> class="review_highlight"<?php endif;?>>Do you require a special diet: <?php echo yes_no($application_details->special_diet);?> <a href="<?php echo base_url();?>form/medical#special-diet" class="show_when_highlighted">fix this</a></span><br/>
      <?php if($application_details->special_diet == 1):?><span<?php if($percent_complete->requirements_met->medical->special_diet_note == 0):?> class="review_highlight"<?php endif;?>>Explain: <?php echo $application_details->special_diet_note;?> <a href="<?php echo base_url();?>form/medical#special-diet-followup" class="show_when_highlighted">fix this</a></span><br/><?php endif;?>
      <span<?php if($percent_complete->requirements_met->medical->other_special_health_needs == 0):?> class="review_highlight"<?php endif;?>>Do you have any other special health needs: <?php echo yes_no($application_details->other_special_health_needs);?> <a href="<?php echo base_url();?>form/medical#special-needs" class="show_when_highlighted">fix this</a></span><br/>
      <?php if($application_details->other_special_health_needs == 1):?><span<?php if($percent_complete->requirements_met->medical->other_special_health_needs_note == 0):?> class="review_highlight"<?php endif;?>>Explain: <?php echo $application_details->other_special_health_needs_note;?> <a href="<?php echo base_url();?>form/medical#special-needs-followup" class="show_when_highlighted">fix this</a></span><br/><?php endif;?>
    </p>
  </div>

  <div class="span12">
    <?php $medical_conditions = get_medical_conditions($application_details);?>
    <p>
      <span<?php if(/*$percent_complete->requirements_met->medical->email_address == 0*/FALSE):?> class="review_highlight"<?php endif;?>>Medical Conditions: <?php echo ($medical_conditions)?$medical_conditions:'None';?> <a href="<?php echo base_url();?>form/contact-information#email-address" class="show_when_highlighted">fix this</a></span>
    </p>
  </div>

  </section>
  <?php endif;?>
    <section class="cf">
      <h3>Contact Information</h3>
      <div class="span6">
        <p>
          <span<?php if((
            $percent_complete->requirements_met->contact_information->address_line_1 +
            $percent_complete->requirements_met->contact_information->city +
            $percent_complete->requirements_met->contact_information->state +
            $percent_complete->requirements_met->contact_information->zip +
            $percent_complete->requirements_met->contact_information->country) < 5):?> class="review_highlight"<?php endif;?>>Your Address:<br/><?php echo $application_details->address_line_1;?><br/>
          <?php if(strlen($application_details->address_line_2) > 0) {echo $application_details->address_line_2.'<br/>';}?>
          <?php echo $application_details->city. ', '.$application_details->state.'   '.$application_details->zip;?><br/>
          <?php echo $application_details->country;?> <a href="<?php echo base_url();?>form/contact-information#address" class="show_when_highlighted">fix this</a></span>
        </p>
      </div>
      <div class="span6 col">
        <p>
          <span<?php if($percent_complete->requirements_met->contact_information->email_address == 0):?> class="review_highlight"<?php endif;?>>Email address: <?php echo $application_details->email_address;?> <a href="<?php echo base_url();?>form/contact-information#email-address" class="show_when_highlighted">fix this</a></span><br/>
          Cell phone: <?php echo (strlen($application_details->cell_phone)>0)?$application_details->cell_phone:'None';?>
          <span<?php if(strlen($application_details->cell_phone) > 0 && $percent_complete->requirements_met->contact_information->cell_phone_carrier == 0):?> class="review_highlight"<?php endif;?>><?php if(strlen($application_details->cell_phone_carrier) > 0){echo ' ('.$application_details->cell_phone_carrier.')';}?> <a href="<?php echo base_url();?>form/contact-information#cell-phone" class="show_when_highlighted">fix this</a></span><br/>
          <span<?php if($percent_complete->requirements_met->contact_information->show_in_directory == 0):?> class="review_highlight"<?php endif;?>>Listed in the HCP directory: <?php echo yes_no($application_details->show_in_directory);?> <a href="<?php echo base_url();?>form/contact-information#show-in-directory" class="show_when_highlighted">fix this</a></span>
        </p>
      </div>
    </section>

    <section class="cf">
      <h3>Ecclesiastical Leader</h3>
      <p>
        <span<?php if(!$ecclesiastical_leader_details):?> class="review_highlight"<?php endif;?>>Ecclesiastical leader name: <?php echo ($ecclesiastical_leader_details)?$ecclesiastical_leader_details->calling.' '.$ecclesiastical_leader_details->last_name:'None <a href="'.base_url().'form/ecclesiastical-leader" class="show_when_highlighted">fix this</a></span>';?><br/>
        <?php if($ecclesiastical_leader_details):?>
          <?php echo ($ecclesiastical_leader_details->calling == 'Bishop')?'Ward':'Branch';?> name: <?php echo $ecclesiastical_leader_details->unit_name;?><br/>
          Stake name: <?php echo $ecclesiastical_leader_details->stake_name;?><br/>
          Endorsement status: <?php echo endorsement_status($application_details);?>
          <?php if($application_details->apply_for_workcrew == 1):?></span><br/>
            <span>Letter of Recommendation: <?php echo workcrew_letter_of_recommendation_status($application_details);?> <a href="<?php echo base_url();?>form/experience#letter-of-recommendation" class="show_when_highlighted">fix this</a></span>
          <?php endif;?>
        <?php endif;?></span>
      </p>
    </section>
<?php endif;?>
    <section class="cf">
      <h3>Experience</h3>
      <p>
        <span<?php if($percent_complete->requirements_met->experience->qual_theater == 0):?> class="review_highlight"<?php endif;?>>Theater experience: <?php echo yes_no($application_details->qual_theater);?> <a href="<?php echo base_url();?>form/experience#theater" class="show_when_highlighted">fix this</a></span><br/>
        <span<?php if($percent_complete->requirements_met->experience->qual_music == 0):?> class="review_highlight"<?php endif;?>>Musical talents: <?php echo yes_no($application_details->qual_music);?> <a href="<?php echo base_url();?>form/experience#music" class="show_when_highlighted">fix this</a></span><br/>
        <span<?php if($percent_complete->requirements_met->experience->qual_it == 0):?> class="review_highlight"<?php endif;?>>IT experience: <?php echo yes_no($application_details->qual_it);?> <a href="<?php echo base_url();?>form/experience#it" class="show_when_highlighted">fix this</a></span><br/>
        <?php if($application_details->qual_theater || $application_details->qual_leadership || $application_details->qual_music || $application_details->qual_it):?>
        <span<?php if($percent_complete->requirements_met->experience->qualifications == 0):?> class="review_highlight"<?php endif;?>>Experience Details:
          <?php echo (strlen($application_details->qualifications)>0)?$application_details->qualifications:'None';?> <a href="<?php echo base_url();?>form/experience#qualifications" class="show_when_highlighted">fix this</a></span><br/>
        <?php endif;?>
        <span<?php if($percent_complete->requirements_met->experience->health_professional == 0):?> class="review_highlight"<?php endif;?>>Health care professional: <?php echo yes_no($application_details->health_professional);?> <a href="<?php echo base_url();?>form/experience#health" class="show_when_highlighted">fix this</a></span><br/>
        <?php if($application_details->health_professional==1):?>
          <span<?php if($percent_complete->requirements_met->experience->profession == 0):?> class="review_highlight"<?php endif;?>>Profession: <?php echo $application_details->profession;?> <a href="<?php echo base_url();?>form/experience#health" class="show_when_highlighted">fix this</a></span><br/>
          <span<?php if($percent_complete->requirements_met->experience->state_of_licensure == 0):?> class="review_highlight"<?php endif;?>>State of licensure: <?php echo $application_details->state_of_licensure;?> <a href="<?php echo base_url();?>form/experience#health" class="show_when_highlighted">fix this</a></span><br/>
        <?php endif;?>
        <span<?php if($percent_complete->requirements_met->experience->qual_languages == 0):?> class="review_highlight"<?php endif;?>>Speaks a foreign language: <?php echo yes_no($application_details->qual_languages);?> <a href="<?php echo base_url();?>form/experience#foreign-language" class="show_when_highlighted">fix this</a></span>
        <?php if ($application_details->qual_languages==1):?><br/>
          <span>Language(s): <?php echo $application_details->other_languages;?> <a href="<?php echo base_url();?>form/preliminary-questions#apply-for" class="show_when_highlighted">fix this</a></span>
        <?php endif;?><br/>
        <span<?php if($application_details->apply_for_workcrew && $percent_complete->requirements_met->experience->comments == 0):?> class="review_highlight"<?php endif;?>>Personal Letter of Recommendation  (<?php echo ($application_details->apply_for_workcrew == 1)?'required':'optional';?>): <?php echo (strlen($application_details->comments)>0)?$application_details->comments:'None <a href="<?php echo base_url();?>form/experience#letter-of-recommendation" class="show_when_highlighted">fix this</a>';?></span><br/>
        Church callings: <?php echo (strlen($application_details->church_callings)>0)?$application_details->church_callings:'None';?><br/>
        Past Pageants: <?php if($past_pageants){foreach($past_pageants as $past_pageant){echo $past_pageant.'  ';}}else{echo 'None';}?>
      </section>

      <section class="cf">
        <h3>Miscellaneous</h3>
        <p>
          <?php if($application_details->apply_for_cast || $application_details->apply_for_staff):?>
          Special Needs: <?php echo (strlen($application_details->special_needs))?$application_details->special_needs:'None';?><br/>
          <span<?php if($percent_complete->requirements_met->miscellaneous->housing_preference == 0):?> class="review_highlight"<?php endif;?>>Housing plans: <?php echo (strlen($application_details->housing_preference)>0)?$application_details->housing_preference:'None';?> <a href="<?php echo base_url();?>form/misc#housing" class="show_when_highlighted">fix this</a></span><br/>
          Willing to be a speaker at the Pageant: <?php echo yes_no($application_details->qual_speaking);?>
          <?php if($application_details->qual_speaking == 1):?><br/><span<?php if($percent_complete->requirements_met->miscellaneous->speaking_topic_or_experience == 0):?> class="review_highlight"<?php endif;?>>Speaking topics or experiences: <?php echo (strlen($application_details->speaking_topic_or_experience) > 0)?$application_details->speaking_topic_or_experience:'None';?> <a href="<?php echo base_url();?>form/misc#speaking_topic_or_experience" class="show_when_highlighted">fix this</a></span><br/><?php endif;?>
          <?php if((int) $application_details->single == 0):?><br/>
            Willing to serve as a Cast Team Leader for:
            <div class="cf">
              <div class="span4">
                <p>
                  <b>Primary</b><br/>
                  Boys &amp; Girls Ages 4-8: <?php echo yes_no($application_details->boys_girls_4_8);?><br/>
                  Girls Age 9: <?php echo yes_no($application_details->girls_9);?><br/>
                  Boys Age 9: <?php echo yes_no($application_details->boys_9);?><br/>
                  Girls Ages 10-11: <?php echo yes_no($application_details->girls_10_11);?><br/>
                  Boys Ages 10-11: <?php echo yes_no($application_details->boys_10_11);?>
                </p>
              </div>

              <div class="span4 col">
                <p>
                  <b>Youth</b><br/>
                  Girls Ages 12-13: <?php echo yes_no($application_details->girls_12_13);?><br/>
                  Boys Ages 12-13: <?php echo yes_no($application_details->boys_12_13);?><br/>
                  Youth Ages 14-15: <?php echo yes_no($application_details->youth_14_15);?><br/>
                  Youth Ages 16-17: <?php echo yes_no($application_details->youth_16_17);?>
                </p>
              </div>

              <div class="span4 col">
                <p>
                  <b>Adult</b><br/>
                  Young Single Adults: <?php echo yes_no($application_details->young_single_adult);?><br/>
                  Adults: <?php echo yes_no($application_details->adult_cast_team);?>
                </p>
              </div>
            </div>
        <?php endif;?>
        <?php endif;?>
        <?php if($application_details->apply_for_workcrew == 1 && $application_details->apply_for_staff != 1 && $application_details->apply_for_cast != 1):?>
          Not applicable to work crew applications.
        <?php endif;?>
        </p>
      </section>

      <section class="cf">
        <form action="<?php echo current_url();?>" method="post">
          <input type="submit" name="mark_as_reviewed" <?php if($percent_complete->preselection->total < 1){echo ' class="disabled" disabled=disabled';}?> value="Mark as Reviewed"/>
        </form>
      </section>
    </article>
