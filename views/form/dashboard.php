<?php
$mode = $this->config->item('mode');
$svg_path = $this->config->item('svg_path');
$INDIVIDUAL_ids = array();

function have_relatives_not_listed($relatives, $INDIVIDUAL_ids){
  foreach ($relatives as $name=>$relative_data) {
    if(!isset($INDIVIDUAL_ids[$relative_data->INDIVIDUAL_id])){
      return(TRUE);
    }
  }
  return(FALSE);
}

# This will hold the application ids which are used for submitting applications
$application_ids = '';
$can_submit = FALSE;
?>
<!--<article class="columns one">&nbsp;</article>-->
<article class="card container">
  <h2 style="margin-bottom:20px;">Application Dashboard</h2>
  <?php if(isset($success_message)):?>
    <div class="autofade" id="success_message"><?php echo $success_message;?></div>
  <?php elseif (isset($error_message)):?>
    <div class="autofade" id="error_message"><?php echo $error_message;?></div>
  <?php endif;?>
  <form class="hidden" id="form" action="<?php echo current_url();?>" method="post"><input id="action" name="action" type="hidden"/><input id="APPLICATION_id" name="APPLICATION_id" value="" type="hidden" /><input id="APPLICATION_INDIVIDUAL_id" name="APPLICATION_INDIVIDUAL_id" type="hidden"/></form>
  <div class="scroll_x">
    <table class="full_width">
      <tr><td>Individual</td><td>Applying for</td><td>Status</td><td>Last Modified</td><td class="center">Action</td></tr>
      <?php foreach ($applications as $key => $app):
        # Check to make sure the application(s) are in a state ready to submit
        if($app->status == 'Reviewed'){
          $can_submit = TRUE;
          # Add the application id deliminated with a pipe
          $application_ids .= $app->id.'|';
        }?>
        <?php $HOUSEHOLD_id = $app->HOUSEHOLD_id; $INDIVIDUAL_ids[$app->INDIVIDUAL_id]=1;?>
        <tr>
          <td>
            <a href="<?php echo (!$accepting_applications)?'#': base_url().'form/head-of-household/'.$app->HOUSEHOLD_id.'/'.$app->INDIVIDUAL_id;?>"><img class="svg <?php echo ($app->head_of_household==0)?'no_dot':'dot';?>" style="vertical-align:middle;" height="24" src="<?php echo ($app->head_of_household==0)?$svg_path.'no_dot.svg':$svg_path.'dot.svg';?>"/></a> <?php echo $app->first_name.' '.$app->last_name;?></td>
            <td><?php echo applying_for($app);?></td>
            <td><?php echo $app->status;?></td>
            <td><?php echo $app->last_modified;?></td>
            <td>
              <a style="margin-bottom:10px;" data-aid = '<?php echo $app->id;?>' data-iid='<?php echo $app->INDIVIDUAL_id;?>' class="<?php echo (!$accepting_applications)?'disabled button':'edit button';?>">Edit</a>
              <a data-aid = '<?php echo $app->id;?>' data-iid='<?php echo $app->INDIVIDUAL_id;?>' class="<?php echo (!$accepting_applications || $mode=="post-selection")?'disabled button':'delete button';?>">Delete</a>
            </td>
          </tr>
        <?php endforeach;?>
      </table>
    </div>
    <p><img class="svg <?php echo ($app->head_of_household==0)?'no_dot':'dot';?>" style="vertical-align:middle;" height="24" src="<?php echo $svg_path.'dot.svg';?>"/> Indicates the head of household</p>

    <?php if($accepting_applications && $relatives && have_relatives_not_listed($relatives, $INDIVIDUAL_ids)):?>
      <div id="relatives_container">
        <h3>Relatives Found</h3>
        <form class="hidden" id="add_relative_form" action="<?php echo base_url();?>form/add-relative" method="post">
          <input id="relative_id" name="INDIVIDUAL_id" type="hidden"/>
          <input name="HOUSEHOLD_id" type="hidden" value="<?php echo $HOUSEHOLD_id;?>"/>
        </form>
        <p>It looks like you applied to the Hill Cumorah Pageant in the past with other memers of your family.  To save time, you can add them to this application by clicking their name.<br/>
        <?php foreach ($relatives as $name=>$relative_data): if(!isset($INDIVIDUAL_ids[$relative_data->INDIVIDUAL_id])):?>
          <a class="add_relative" data-iid="<?php echo $relative_data->INDIVIDUAL_id;?>" href="#"><?php echo $name;?></a> &nbsp;
        <?php endif; endforeach;?></p>
        <hr/>
      </div>
      <?php endif;?>
      <form action="<?php echo base_url();?>form/start-from-scratch" method="post">
        <input type="hidden" name="HOUSEHOLD_id" value="<?php echo $HOUSEHOLD_id;?>" type="hidden"/>
        <p><input<?php if(!$accepting_applications || $mode == "post-selection"){echo ' disabled="disabled" class="disabled"';}?> name="submit" type="submit" value="Start New Individual"/></p>
      </form>

      <form action="<?php echo current_url();?>" method="post">
        <input type="hidden" name="application_ids" value="<?php echo rtrim($application_ids,'|');?>" type="hidden"/>
        <input type="hidden" name="action" value="submit" type="hidden"/>
        <input<?php if(!$can_submit){echo ' disabled="disabled" class="disabled"';}?> name="submit" type="submit" value="Submit Application<?php if(sizeof($applications)>1){echo 's';};?>"/>
      </form>

    </div>

  </article>

  <script>
  // Function for handling the action buttons
  function take_action(action, APPLICATION_id, APPLICATION_INDIVIDUAL_id){
    $('#APPLICATION_id').val(APPLICATION_id);
    $('#APPLICATION_INDIVIDUAL_id').val(APPLICATION_INDIVIDUAL_id);
    $('#action').val(action);
    $('#form').submit();
  }

  $('.edit').click(function(){
    take_action('edit', $(this).attr('data-aid'), $(this).attr('data-iid'));
  });

  $('.delete').click(function(){
    do_it = confirm("Do you really want to delete this individual's application?  Note: You will have to contact the cast administrator to restore the application.")
    if(do_it == true){
      take_action('delete', $(this).attr('data-aid'), $(this).attr('data-iid'));
    }
  });

  $('a.add_relative').click(function(){
    $('#relative_id').val($(this).attr('data-iid'));
    $('#add_relative_form').submit();
  });

  setTimeout(function(){$('div.autofade').fadeOut()}, 5000);
  </script>
