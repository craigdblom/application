<?php echo $sidebar;?>
<article class="card nine columns" id="application_form">
  <h2>Participation Agreement</h2>
  <?php if(isset($error_message)):?>
    <div id="error_message"><?php echo $error_message;?></div>
  <?php endif;?>
  <form action="<?php echo current_url();?>" method="post" id="form">
    <section class="span12 col cf">
        <p>In order to apply to participate you must agree to the following:</p>
        <form action="<?php echo current_url();?>" method="post">
            <table>
 <tr style="display:none;"><td colspan="2"></td></tr>
 <tr style="border-top:1px solid #E0E0E0;">
     <td valign="top"><a name="participation_agreement"></a>
         <input id="agreed_to_communication" class="checkbox" type="checkbox" name="agreed_to_communication" value="1"<?php if($application_details->agreed_to_communication){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /></td><td valign="top"> I agree to receive electronic communication from the Hill Cumorah Pageant.
    </td>
</tr>
<tr>
    <td valign="top"><a name="apply-for"></a>
        <input id="agree_store_and_use_data" class="checkbox" type="checkbox" name="agree_store_and_use_data" value="1"<?php if($application_details->agree_store_and_use_data){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /></td><td valign="top"> I agree for the Hill Cumorah Pageant to store and use all personal data I submit for the purposes of my participation in the Pageant.
    </td>
</tr>
<tr>
    <td valign="top"><a name="apply-for"></a>
        <input id="agree_legal_guardian" class="checkbox" type="checkbox" name="agree_legal_guardian" value="1"<?php if($application_details->agree_legal_guardian){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /></td><td valign="top"> I certify that I and/or my spouse are the legal guardian of all applicants listed in our  household application under the age of 18.
    </td>
 </tr>
 <tr>
     <td valign="top"><a name="agree_physically_fit"></a>
         <input id="agree_physically_fit" class="checkbox" type="checkbox" name="agree_physically_fit" value="1"<?php if($application_details->agree_physically_fit){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /></td><td valign="top"> I certify that no one in my household application has physical, emotional or mental limitations that would prevent them from taking and understanding direction, walking quickly up and down stairs, kneeling and rising without assistance and navigating rough ground in the dark other than as noted.
    </td>
</tr>
<tr>
    <td valign="top"><a name="agree_no_pets"></a>
        <input id="agree_no_pets" class="checkbox" type="checkbox" name="agree_no_pets" value="1"<?php if($application_details->agree_no_pets){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /></td><td valign="top"> I understand that I cannot bring pets or service animals to the grounds of the Hill Cumorah Pageant.
    </td>
</tr>
<tr>
    <td valign="top"><a name="apply-for"></a>
        <input id="agree_to_ecclesiastical_contact" class="checkbox" type="checkbox" name="agree_to_ecclesiastical_contact" value="1"<?php if($application_details->agree_to_ecclesiastical_contact){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /></td><td valign="top"> I certify that all applicants are members of the Church of Jesus Christ of Latter-day Saints and understand that our ecclesiastical leaders will be contacted for a temple worthiness endorsement as a prerequisite to participate as a cast member in the Hill Cumorah pageant.
    </td>
</tr>
<tr>
    <td valign="top"><a name="apply-for"></a>
        <input id="agree_both_parents" class="checkbox" type="checkbox" name="agree_both_parents" value="1"<?php if($application_details->agree_both_parents){echo ' checked="checked"';}?> data-endpoint="application/<?php echo $_SESSION['APPLICATION_id'];?>" /></td><td valign="top"> I understand that in order for a family to participate as part of the cast in the Pageant, both parents must apply and be in attendance throughout the pageant.  Single head of household families are allowed.
    </td>
</tr>
</table>
<input name="save_and_continue" type="submit" value="Save and Continue"/>
</section>
</form>
</section>




    <script type="text/javascript" src="<?php echo($this->config->item('js_path'));?>jquery.cycle.all.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.slideshow').cycle({
        fx: 'fade' // choose your transition type, ex: fade, scrollUp, shuffle, etc...
      });
    });
    </script>

  </div><br/><?php $_SESSION['redirect_after_crop'] = current_url();?>

</section>



</form>
</article>
<script>
var ACCOUNT_ID = <?php echo $_SESSION['ACCOUNT_id']?>;
var TOKEN = '<?php echo $_SESSION['token'];?>';


function strip_double_quotes(vars){
  if(typeof vars === 'string' || vars instanceof String){
    return vars.replace(/"/g, '&quot;');
  } else {
    return vars;
  }
}

function update_percent_complete(){
  var url = '<?php echo $this->config->item('api_url');?>v2/application/<?php echo $_SESSION['APPLICATION_id'];?>/percent-complete';
  var data = jQuery.parseJSON('{"api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    dataType: 'json',
    success: function(response){
      percent = Math.round(response.data.preselection.total*100);
      animate();
    }
  });
};


function autosave(name, val, url){
  val = strip_double_quotes(val);
  $('#savebar').addClass('saving');
  $('#savebar').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
    $('#savebar').removeClass('saving');
  });
  var data = jQuery.parseJSON('{"'+name+'": "'+val+'", "api_credentials":{"ACCOUNT_id":"'+ACCOUNT_ID+'", "token":"'+TOKEN+'"}}');
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    dataType: 'json',
    success: function(data){
      // Sucess
      $('[name="'+name+'"]').removeClass('has_error');
      $('#'+name+'_error').remove();
      update_percent_complete();
    },
    error: function(data){
      // Failure to save data
      response = data.responseJSON;
      data = response.data;
      $('#savebar').removeClass('saving').addClass('saved');
      $('[name="'+name+'"]').addClass('has-error');
      error_id = name+'_error';
      if($("#"+error_id).length == 0){
        $('<span id="'+error_id+'" class="error_message">'+data.error_message+'</span>').insertAfter('[name="'+name+'"]');
      }
    }
  });
};


$('.autosave').change(function(){
  name = $(this).attr('name');
  val = $(this).val();
  autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
  autosave(name, val, autosave_url);
});


$(".checkbox").click(function(){
  var is_clicked = $(this).is(':checked');
  var name = $(this).attr('name');
  if(is_clicked){
    var val = 1;
  } else {
    var val = 0;
  }
  autosave_url = '<?php echo $this->config->item('api_url');?>v2/'+$(this).attr('data-endpoint');
  autosave(name, val, autosave_url);
});

$('#save_and_continue_button').click(function(){
  $('#form').submit();
});
</script>
