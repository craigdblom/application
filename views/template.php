<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$svg_path = $this->config->item('svg_path');
?><!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <?php if(current_url() != $this->config->item('sso_url')):?>
	<meta http-equiv="refresh" content="900;url=<?php echo $this->config->item('auto_sign_out_url');?>" />
  <?php endif;?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/png" href="<?php echo $this->config->item('img_path');?>favicon.png"/>
	<!-- FONT -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
	<!-- CSS Files -->
	<?php $css_path = $this->config->item('css_path'); foreach ($this->config->item('css_files') as $css_file):?><link rel="stylesheet" href="<?php echo $css_path.$css_file.'?'.md5(time());?>" type="text/css" />
	<?php endforeach;?><!-- JavaScript Files -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/jquery-3.2.1.min.js"><\/script>')</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.5.3/modernizr.min.js"></script>
	<?php $js_path = $this->config->item('js_path'); foreach ($this->config->item('js_files') as $js_file):?><script src="<?php echo $js_path.$js_file;?>"></script>
<?php endforeach;?></head>
	<title>Hill Cumorah Pageant <?php echo (isset($title))? ' : '.$title:'';?></title>
</head>

<body class="cf<?php if(isset($body_class)){echo ' '.$body_class;}?>">
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-85193311-3', 'auto');
    ga('send', 'pageview');
  </script>

  <header>
    <a href="<?php echo $this->config->item('header_hcp_url');?>">
      <h1 class=""><img src="<?php echo $svg_path;?>hcp.svg" width="28" /> &nbsp;Hill Cumorah Pageant</h1>
    </a>
    <nav style="float:right;">
      <ul class="clearfix">
        <?php if(isset($_SESSION['signed_in'])):?>
          <li>
            <span class="hidden_to_mobile">
              <a id="signed_in" class="rounded_image" name="#"><img src="<?php $image = ($_SESSION['profile_image_uploaded']=="1")?$_SESSION['INDIVIDUAL_id']:'no_image'; echo $this->config->item('img_path') . $image.'.jpg?'.md5(time());?>" /></a>
              <?php echo $_SESSION['first_name'];?></span>
              <a id="dot_dot_dot"><span id="dot_dot_dot_button">&vellip;</span></a>
            </li>
          <?php endif;?>
        </ul>
      </nav>
    </header>

    <?php if(isset($_SESSION['signed_in'])):?>
      <ul id="dot_dot_dot_menu" class="hidden">
        <li><a href="<?php echo $this->config->item('settings_url');?>">Settings</a></li>
        <li><a href="<?php echo $this->config->item('sign_out_url');?>">Sign Out</a></li>
        <?php if($_SESSION['cast_admin'] || $_SESSION['workcrew_admin'] || isset($_SESSION['switch_back'])): if(isset($_SESSION['switch_back'])):?>
          <li><a href="<?php echo $this->config->item('header_hcp_url');?>sso/switch-back">Switch Back</a></li>
        <?php else:?>
          <li><a href="<?php echo $this->config->item('header_hcp_url');?>sso/switch-to">Switch Account</a></li>
        <?php endif; endif;?>
      </ul>

    <?php endif;?>
    <div id="savebar"></div>
    <main>
  <div id="container" class="cf">
    <?php echo $application;?>
  </div> <!-- end of #container -->
</main>
<script>
$(document).ready(function(){
  $('#dot_dot_dot').click(function(){
    is_white = $(this).hasClass('white');
    if(is_white){
      $(this).removeClass('white');
      $('#dot_dot_dot_menu').addClass('hidden');
    } else {
      $(this).addClass('white');
      $('#dot_dot_dot_menu').removeClass('hidden');
    }
  });
});
</script>
  <footer>
    <p class="center">&copy; <?php echo date('Y');?> Hill Cumorah Pageant. All Rights Reserved.</p>
  </footer>
</body>
</html>
