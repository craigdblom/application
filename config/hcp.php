<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Hill Cumorah Pageant Specific Config Settings
|--------------------------------------------------------------------------
| 1. CSS, JavaScript and Image Settings
| 2. Key Hill Cumorah Pageant URLs
*/
$server = 'http://localhost/';

/*
|--------------------------------------------------------------------------
| 1. CSS, JavaScript and Image Settings
|--------------------------------------------------------------------------
| Path variables are added to the base URL.  It MUST have the trailing slash!
| File names will be added to the base URL + path.  It is recommended to use
| version numbers to deal with pushing changes in css and js files to end users.
| It get's around browser caching issues.
*/

# CSS Specific Settings
$config['css_path'] = $server.'assets/css/';
$config['css_files'] = array('normalize-3.0.2.css', 'skeleton-2.0.4.hcp.css', 'hcp-1.1.0.css', 'remodal-1.1.1.css', 'remodal-default-theme-1.1.1.css');

# JavaScript Specific Settings
$config['js_path'] = $server.'assets/js/';
$config['js_files'] = array('jquery-3.1.0.min.js', 'remodal-1.1.1.min.js');

# Image Specific Settings
$config['img_path'] = $server.'assets/img/';

# PDF Settings
$config['pdf_path'] = $server.'assets/pdf/';

# SVG Specific Settings
$config['svg_path'] = $server.'assets/svg/';

/*
|--------------------------------------------------------------------------
| 2. Key Hill Cumorah Pageant URLs
|--------------------------------------------------------------------------
*/
$config['api_url'] = $server.'api/';
$config['sso_url'] = $server.'account/sso';
$config['header_hcp_url'] = $server.'account/';
$config['sign_out_url'] = $config['header_hcp_url'].'sign-out';
$config['settings_url'] = $config['header_hcp_url'].'settings';
$config['auto_sign_out_url'] = $config['header_hcp_url'].'sso/auto-sign-out';
$config['application_start_url'] = $server.'application/start';

/*
|--------------------------------------------------------------------------
| 3. Accepting Applications Flag
|--------------------------------------------------------------------------
| Set to TRUE to allow individuals to make edits to their application.  If
| FALSE the user will have disabled buttons.
*/
$config['accepting_applications'] = TRUE;

/*
|--------------------------------------------------------------------------
| 4. Dashboard mode
|
| Valid modes: application, selection, post-selection
|--------------------------------------------------------------------------
*/

$config['mode'] = 'application';
